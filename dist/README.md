<!--suppress HtmlDeprecatedAttribute, HtmlUnknownTarget -->
# Foundry VTT suomenkielinen käännös (Finnish Translation)
[![FVTT Version](https://img.shields.io/badge/dynamic/json.svg?url=https%3A%2F%2Fgitlab.com%2Ffoundry-vtt-suomi%2Ffvtt-lang-fi-fi%2F-%2Fraw%2F17.0.1%2Fdist%2Flang-fi-fi%2Fmodule.json&label=FVTT%20Version&query=$.compatibility.verified&colorB=orange)](https://foundryvtt.com/ "Foundry Virtual Tabletop verkkosivu") [![Module Version](https://img.shields.io/badge/dynamic/json.svg?url=https%3A%2F%2Fgitlab.com%2Ffoundry-vtt-suomi%2Ffvtt-lang-fi-fi%2F-%2Fraw%2F17.0.1%2Fdist%2Flang-fi-fi%2Fmodule.json&label=Module%20Version&query=$.version&color=orange)](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases/permalink/latest/downloads/module.json "Manifestin osoite") [![Manifest+ Version](https://img.shields.io/badge/dynamic/json.svg?url=https%3A%2F%2Fgitlab.com%2Ffoundry-vtt-suomi%2Ffvtt-lang-fi-fi%2F-%2Fraw%2F17.0.1%2Fdist%2Flang-fi-fi%2Fmodule.json&label=Manifest%2B%20Version&query=$.manifestPlusVersion&colorB=blue)](https://foundryvtt.wiki/en/development/manifest-plus "Manifest+ spesifikaatio")
[![MIT License](https://img.shields.io/badge/License-MIT-lightgray)](https://opensource.org/licenses/MIT "MIT License") [![Foundry VTT Suomi Discord Invite](https://img.shields.io/discord/788885085915054091?color=%237289DA&label=Chat&logo=discord&logoColor=white)](https://discord.gg/U4y3cNebbg "Foundry VTT Suomi Discord-kutsu")

Suomenkielinen käännös [Foundry Virtual Tabletop](https://foundryvtt.com/ "Foundry Virtual Tabletop verkkosivu.")&nbsp;-&NoBreak;sovelluksen käyttöliittymälle, karttamuistiinpanojen kuvakkeille, TinyMCE-tekstinkäsittelyohjelmalle, tutustumiskierroksille, sekä näppäimistön näppäimille. Lataa uusimman version arkisto **[lang-fi-fi.zip](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases/17.0.1/downloads/lang-fi-fi.zip "Moduulin ZIP-arkiston suora latauslinkki.")** tai asenna suoraan sovelluksessa manifestin osoitteella:
```
https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases/permalink/latest/downloads/module.json
```
Katso [tarkemmat asennusohjeet](#nopea-asennusohje) alta (mm. TinyMCE käännöstiedoston lataaminen ja päivämäärävalitsimen käännös).

Finnish translation for the [Foundry Virtual Tabletop](https://foundryvtt.com/ "Foundry Virtual Tabletop website.") application user interface, map note icons, TinyMCE text editor, tours, and keyboard keys. Download the newest version's archive **[lang-fi-fi.zip](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases/17.0.1/downloads/lang-fi-fi.zip "Direct download link for the module's ZIP archive.")** or install it directly in the application using the manifest URL above. See [detailed installation instructions](#quick-installation-guide) below (including how to install the TinyMCE translation and translate the date picker). [English README](#in-english-englanniksi) at the end.

![Käännetty ylläpitovalikko.](/dist/img/setup_menu.png)

# Suomeksi (In Finnish)
<img src="/dist/img/module_information_window.png" alt="Käännöksen tietoikkuna." align="right">

Huomaa, että Foundry VTT *ei vielä* tue kaiken tekstin kääntämistä käyttöliittymässä. Kääntämätön teksti näkyy englanniksi. Jos peli on päivittynyt viimeaikoina, annathan kääntäjille aikaa päivittää käännös. Voit katsoa mikä on viimeisin asentamasi käännöksen tukema Foundry VTT:n versio painamalle **Käännöksen tiedot**&nbsp;-&NoBreak;nappia sivupalkin **Peliasetukset**-välilehdeltä, mikäli moduuli on laitettu päälle. Tämä moduuli **ei sisällä** käännöksiä muille paketeille: moduuleille, pelijärjestelmille tai maailmoille.

Yhteisön tekemiä suomenkielisiä käännöksiä moduuleille, pelijärjestelmille ja maailmoille työstetään pääasiassa [Foundry Hub Weblate](https://weblate.foundryvtt-hub.com/languages/fi/) -palvelussa. Joitain yksittäisiä käännöksiä voi olla **Foundry VTT Suomi**&nbsp;-&NoBreak;projektissa [GitLabissa](https://gitlab.com/foundry-vtt-suomi "Foundry VTT Suomi GitLabissa.") ja [GitHubissa](https://github.com/foundry-vtt-suomi "Foundry VTT Suomi GitHubissa"). Ota yhteyttä kääntäjiin, anna palautetta käännöksestä, tai ilmoita käännöksen ongelmista [Foundry VTT Suomi&nbsp;-&NoBreak;Discord-palvelimella](https://discord.gg/U4y3cNebbg "Foundry VTT Suomi -Discord-palvelimen kutsulinkki.") Voit myös ilmoittaa ongelmista tämän käännöksen kanssa koodisäilön [ongelmanseuraimessa](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/issues).

Katso myös suomenkielinen versio [Foundry VTT yhteisöwikistä](https://foundryvtt.wiki/fi/home "Suomenkielinen Foundry VTT yhteisöwiki").

## Ominaisuudet
- Käyttöliittymän suomenkielinen käännös. (100% tekstistä, jonka *voi* kääntää, katso koodisäilön [ongelmanseurain](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/issues).)
  - Päivämäärävalitsimen kielen vaihtaminen vaatii pelin pikakuvakkeen muokkausta. ([Katso ohjeet alla.](#1-vaihda-päivämäärävalitsimen-kieli))
- Käännöksen tietoikkuna (sivupalkista **Peliasetukset > Käännöksen tiedot**), joka näyttää mm. onko käännös yhteensopiva nykyisen Foundry VTT version kanssa, onko TinyMCE käännös asennettu, sekä kääntäjien nimet.
- Näppäimistön näppäimien osittainen käännös.

Seuraavat ominaisuudet vaativat moduulin aktivoimisen maailmassa. ([Katso ohjeet alla.](#2-aktivoi-moduuli))
- [TinyMCE](https://www.tiny.cloud/ "TinyMCE verkkosivu.")-tekstinkäsittelyohjelman käyttöliittymän suomenkielinen käännös: kielen vaihtaminen vaatii erillisen käännöstiedoston lataamisen ([ohjeet alla](#3-lataa-tinymce-k%C3%A4%C3%A4nn%C3%B6s)).
  - Pelin TinyMCE:hen lisäämien valikkojen käännös.
  - TinyMCE-lisäosien valikkojen käännös. (Jotka *voi* kääntää, katso koodisäilön [ongelmanseurain](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/issues).)
  - Kun TinyMCE päivittyy, uudet kääntämättömät ominaisuudet näkyvät englanniksi.
- Karttamuistiinpanojen kuvakkeiden nimien käännös.
  - Kun kuvakkeet päivittyvät, uudet kääntämättömät nimet näkyvät englanniksi.
- Tutustumiskierrosten käännös.
- Kattavampi näppäimistön näppäimien käännös. Ominaisuuden saa pois päältä moduulin asetuksista, mikäli halutaan säilyttää näppäimien nimet osittain englanniksi.

## Nopea asennusohje
Mikäli sinulle kelpaa osittainen käyttöliittymän käännös, niin pelkkä moduulin asentaminen ja kielen vaihtaminen asetuksista riittää. Tarkemmat ohjeet tähän on alla.

### 1. Asenna moduuli
Helpoiten asennus onnistuu asentamalla moduuli `Translation: Suomi [ydin] | Finnish [Core]` pelin liitännäismoduulien asentajasta (**Add-On Modules**&nbsp;-&NoBreak;välilehdeltä), jolloin saat uusimman version, joka on yhteensopiva sinun Foundry VTT versiosi kanssa. Mikäli haluat asentaa käännöksen tietylle vanhemmalle Foundry VTT:n versiolle käsin, löydät kaikki moduulin julkaistut versiot [Releases](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases)-sivulta.

Käännöksen uusimman version manifestin osoite on:
```
https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases/permalink/latest/downloads/module.json
```
Voit myös ladata uusimman ZIP-arkiston **[lang-fi-fi.zip](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases/17.0.1/downloads/lang-fi-fi.zip "Moduulin ZIP-arkiston suora latauslinkki.")** ja purkaa sen sisällön hakemistoon: `<käyttäjäresurssien polku>/Data/modules/`

Käyttäjäresurssien polku löytyy ylläpitovalikon **Configuration** välilehdellä **User Data Path**&nbsp;-&NoBreak;kohdasta tai avaamalla Foundry VTT, napsauttamalla sovelluksen kuvaketta tehtäväpalkissa oikealla hiiren napilla ja valitsemalla **Browse User Data**.

### 2. Vaihda Foundry VTT:n kieli
Moduulin asentamisen jälkeen Foundry VTT:n *oletuskielen*, joka näkyy ylläpitovalikossa johon peli käynnistyy, voi vaihtaa suomeksi ylläpitovalikon **Configuration** välilehdeltä kohdasta **Default Language** valitsemalla `suomi (Finnish) - Suomi [ydin] | Finnish [Core]`. Tämän jälkeen sovellus pitää käynnistää uudelleen, jotta kieli vaihtuu.

Foundry VTT:n maailmoissa näkyvä *kieli* vaihdetaan avaamalla maailma ja valikosta **Game Settings > Configure Settings** valitse **Language Preference** kohdasta kieleksi `suomi (Finnish)`. Jos kielen vaihtaminen maailman asetuksissa ei muuta käyttöliittymän kieltä maailmassa, pyydä pelinjohtajaasi asentamaan ja/tai aktivoimaan tämä moduuli maailmassa.

## Laajennettu asennusohje
<img src="/dist/img/date_picker.png" alt="Käännetty päivämäärävalitsin." align="right">

Mikäli haluat kokonaisemman käännöksen, seuraa alla olevia ohjeita yllä olevan [nopean asennusohjeen](#nopea-asennusohje) *lisäksi*.

### 1. Vaihda päivämäärävalitsimen kieli
Päivämäärävalitsimen (joka löytyy esimerkiksi maailman muokkausikkunasta) kieli vaihtuu käyttöjärjestelmän kielen mukaan. Voit joko vaihtaa käyttöjärjestelmäsi kieleksi suomen tai muokata Foundry VTT:n pikakuvakkeen polkua.
1. Klikkaa sovelluksen kuvaketta oikealla hiiren napilla, valitse **Properties** (tai vastaava).
1. Etsi kuvakkeen polku (esim. **Target**-kenttä) ja lisää sen perään ` --lang=FI`, esimerkiksi tähän tapaan:
```
"C:\Foundry VTT\FoundryVTT.exe" --lang=FI
```

### 2. Aktivoi moduuli
Tämä moduuli "`Suomi [ydin] | Finnish [Core]`" tulee aktivoida jokaisessa maailmassa, jossa haluat TinyMCE-tekstinkäsittelyohjelman ja karttamuistiinpanojen kuvakkeet suomeksi. Vain **pelinjohtajat** voivat aktivoida moduuleja. Aktivointi tapahtuu ruudun oikeanpuoleisen valikon kautta **Peliasetukset > Hallitse moduuleja** napilla. Valitse `Suomi [ydin] | Finnish [Core]` ja paina **Tallenna muutokset**. Moduulin aktivoiminen myös mahdollistaa tilan, jossa päävalikko on yhdellä kielellä ja maailmassa käyttöliittymä on suomeksi.

<img src="/dist/img/tinymce.png" alt="Käännetty TinyMCE-tekstinkäsittelyohjelma." align="right">

### 3. Lataa TinyMCE käännös
Epäselvän lisensoinnin takia tämä moduuli ei voi sisällyttää TinyMCE-tekstinkäsittelyohjelman suomenkielistä kielipakettia, vaan se pitää ladata erikseen tästä suorasta latauslinkistä: [https://download.tiny.cloud/tinymce/community/languagepacks/6/fi.zip](https://download.tiny.cloud/tinymce/community/languagepacks/6/fi.zip "Suora latauslinkki viralliseen TinyMCE:n suomenkieliseen kielipakettiin.")

Pura `fi.js` tiedosto ladatusta arkistosta polkuun:
```
<käyttäjäresurssien polku>/Data/modules/lang-fi-fi/lang/tinymce/fi.js
```
Päivitä peli (<kbd>F5</kbd>) ladataksesi TinyMCE käännöksen, jos teet tämän kun peli on päällä.

## Usein kysytyt kysymykset
Alla on vastauksia yleisiin ongelmiin, joita voi esiintyä moduulin asentamisen jälkeen.

- Miksi Foundry VTT:n ylläpitovalikko/päävalikko ei ole suomeksi?
- Miksi Foundry VTT ei ole suomeksi kun pelaan peliä maailmassa?
  - Et ole vaihtanut Foundry VTT:n *oletuskieltä* tai *kieltä* suomeksi. Katso ohjeet kohdasta [Vaihda Foundry VTT:n kieli](#2-vaihda-foundry-vttn-kieli).
- Miksi en näe **Käännöksen tiedot**&nbsp;-&NoBreak;nappia sivupalkissa?
- Miksi TinyMCE-tekstinkäsittelyohjelma ei ole suomeksi?
- Miksi karttamuistiinpanojen kuvakkeet eivät ole suomeksi?
  - Kaikki nämä ongelmat ratkeavat aktivoimalla moduuli. Katso ohjeet kohdasta [Aktivoi moduuli](#2-aktivoi-moduuli).

## Tunnetut ongelmat
- Kaikki teksti käyttöliittymässä ei ole käännetty: Foundry VTT ei vielä tue kaiken tekstin kääntämistä. Voimme vain odottaa sovelluksen päivittyvän tulevaisuudessa.
- Kaikki teksti TinyMCE tekstinkäsittelyohjelman käyttölittymässä ei ole käännetty: TinyMCE ja sen lisäosat eivät vielä tue kaiken tekstin kääntämistä. Voimme vain odottaa sovellusten päivittyvän tulevaisuudessa.
- Valitun tekstin raahaaminen käännöksen tietoikkunassa aiheuttaa virheitä selaimen konsolissa: tämä on pieni tekninen virhe, joka ei haittaa moduulin toiminnallisuutta.
- Työkaluvinkki ei näy käännöksen tietoikkunan ulkopuolella: tämä on pieni tekninen virhe, joka ei haittaa moduulin toiminnallisuutta.

## Yhteensopivuus
Tämän suomenkielisen käännöksen yhteensopivuus Foundry VTT:n versioiden kanssa näet [Releases](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases)-sivulta, sekä [CHANGELOG.md](/module/CHANGELOG.md)-tiedostosta. Tavoitteena on julkaista käännös jokaiselle Foundry VTT:n versiolle version 0.7.5 jälkeen.

## Osallistu kääntämiseen
Katso [CONTRIBUTING.md](/CONTRIBUTING.md).

## Kuvankaappauksia
![Käännetty hahmoluettelo.](/dist/img/actors_directory.png)
![Käännetty kohtauksen määritysikkuna.](/dist/img/scene_configuration.png)
![Käännetty karttamuistiinpanon määritysikkuna.](/dist/img/map_note_configuration.png)


# In English (englanniksi)
<img src="/dist/img/module_information_window.png" alt="Translation information window." align="right">

Please note that Foundry VTT *does not yet* support translating all text in the user interface. Untranslated text is in English. If the game has recently updated, please give the translators some time to update the translation. You can see the currently installed translation's latest supported Foundry VTT version by clicking the **Käännöksen tiedot** button in the sidebar's **Peliasetukset** tab if the module has been activated. This module **does not** contain translations for other packages: modules, game systems, or worlds.

For additional Finnish community translations for modules, game systems, and worlds see the [Foundry Hub Weblate](https://weblate.foundryvtt-hub.com/languages/fi/) or the **Foundry VTT Suomi** project on [GitLab](https://gitlab.com/foundry-vtt-suomi "Foundry VTT Suomi GitLab project page.") and [GitHub](https://github.com/foundry-vtt-suomi "Foundry VTT Suomi GitHub organization."). Contact the translators, give feedback, or report issues with this translation on the [Foundry VTT Suomi Discord server](https://discord.gg/U4y3cNebbg "Foundry VTT Suomi Discord server invite link."). You may also report issues with this translation in the repository's [issue tracker](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/issues).

Also, see the Finnish version of the [Foundry VTT Community Wiki](https://foundryvtt.wiki/fi/home "Finnish Foundry VTT Community Wiki").

## Features
- Translation of the user interface. (100% of the text that *can be* translated, see the repository's [issue tracker](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/issues))
  - Changing the date picker widget's language requires modifying the game shortcut. ([See instructions below.](#1-change-the-date-picker-widget-language))
- Translation information window (**Peliasetukset > Käännöksen tiedot** from the sidebar) that shows e.g., is the translation compatible with the current Foundry VTT version, is the TinyMCE translation installed, the names of the translators.
- Partial translation of keyboard keys.

The following features require activating the module in a world. ([See instructions below.](#1-change-the-date-picker-widget-language))
- [TinyMCE](https://www.tiny.cloud/ "TinyMCE website.") rich text editor Finnish translation: requires downloading a separate language pack ([instructions below](#3-download-a-tinymce-language-package))
  - Translates all custom menus added to TinyMCE.
  - Translates all menus added by TinyMCE. (That *can be* translated, see the repository's [issue tracker](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/issues))
  - When TinyMCE updates, newly added untranslated options will be displayed in English.
- Translation of all map note icon names.
  - When map notes update, newly added untranslated icons will be displayed in English.
- Translation of tours.
- A more comprehensive translation of keyboard keys. The feature can be disabled in module settings, if you wish the keep the key names partially in English.

## Quick Installation Guide
If you're fine with a partial user interface translation, simply installing the module and changing the language from the settings is enough. More detailed instructions for this are provided below.

### 1. Install the Module
The easiest way to install the module is to install `Translation: Suomi [ydin] | Finnish [Core]` from the in-game add-on installer (in the **Add-On Modules** tab). This way you will get the latest version of the module for your current Foundry VTT version. If you want to install the translation for a specific older version of Foundry VTT manually you will find all released versions of the module from the [Releases](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases) page.

The manifest URL for the newest module version is:
```
https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases/permalink/latest/downloads/module.json
```
You can also download the newest ZIP archive **[lang-fi-fi.zip](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases/17.0.1/downloads/lang-fi-fi.zip "Direct download link for the module's ZIP archive.")** and extract its contents into the directory: `<user data path>/Data/modules/`

The user data path can be found in the setup menu's **Configuration** tab in the field **User Data Path** or by opening Foundry VTT, clicking the game's icon in the task bar with the right mouse button, and choosing **Browse User Data**.

### 2. Change Foundry VTT Language
After installing the module change Foundry VTT's *default language* (used in the setup menu) to Finnish in the setup menu's **Configuration** tab by choosing `suomi (Finnish) - Suomi [ydin] | Finnish [Core]` as the **Default Language**. After this the application has to be restarted for the language to change.

Change the *language* used in Foundry VTT worlds by launching a world and change the **Preferred Language** to `suomi (Finnish)` from **Game Settings > Configure Settings**. If changing the language in the world settings does not change the user interface language in the world, ask your Game Master to install or enable this module in the current world.

## Complete Installation Guide
<img src="/dist/img/date_picker.png" alt="Translated date picker widget." align="right">

If you want a more complete translation, follow the instructions below *in addition* to the [Quick Installation Guide](#quick-installation-guide) above.

### 1. Change the Date Picker Widget Language
The date picker widget (for example in the edit world window) language changes based on the operatin system language. You can either change your operating system language to Finnish or modify the path of the Foundry VTT shortcut.
1. Click the application shortcut with the right mouse button and choose **Properties** (or similar).
1. Find the shortcut's path (e.g., the **Target** field) and add ` --lang=FI` to the end, gor example, like this:
```
"C:\Foundry VTT\FoundryVTT.exe" --lang=FI
```

### 2. Activate the Module
**In addition to this** the module `Suomi [ydin] | Finnish [Core]` must be activated in every world where you want the TinyMCE text editor and map note icon names to be in Finnish. Only **Game Masters** can activate modules. To activate modules go to **Game Settings > Manage Modules** in the right sidebar. Select `Suomi [ydin] | Finnish [Core]` and press **Save Module Settings**.

If changing the language in world settings does not change the language of the user interface in that world, ask your Game Master to install this module and/or activate it in this world.

<img src="/dist/img/tinymce.png" alt="Translated TinyMCE text editor." align="right">

### 3. Download a TinyMCE Language Package
Due to unclear licensing this module cannot include the Finnish language package for the TinyMCE editor, and it has to be downloaded separately from this direct link: [https://download.tiny.cloud/tinymce/community/languagepacks/6/fi.zip](https://download.tiny.cloud/tinymce/community/languagepacks/6/fi.zip "Direct download link to the official TinyMCE Finnish language package.")

Extract the file `fi.js` from the downloaded archive to the path:
```
<user data path>/Data/modules/lang-fi-fi/lang/tinymce/fi.js
```
Refresh the game (<kbd>F5</kbd>) to load the TinyMCE translation if you do this while the game is running.

## Frequently Asked Questions (FAQ)
- Why is Foundry VTT's setup menu/main menu not in Finnish?
- Why is Foundry VTT not in Finnish when playing a game in a world?
  - You have not changed Foundry VTT's *default language* or *language* to Finnish. See instructions in [Change Foundry VTT Language](#3-change-foundry-vtt-language).
- Why can I not see the **Käännöksen tiedot** button in the sidebar?
- Why is the TinyMCE rich text editor not in Finnish?
- Why are the map note icons not in Finnish?
  - Solve all of these issues by activating the module. See instructions in [Activate the Module](#2-activate-the-module).

## Known Issues
- Some text in the user interface is not translated: Foundry VTT does not support translating all text yet. We can only wait for the game to be updated.
- Some text in the TinyMCE interface is not translated: TinyMCE and its plugins do not support translating all text yet. We can only wait for the game to be updated.
- Dragging text in the module information window causes a console error: this is a minor technical issue that does not affect the functionality of the module.
- The tooltip does not show outside the module information window: this is a minor technical issue that does not affect the functionality of the module.

## Combatibility
The [Releases](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases) page and the [CHANGELOG.md](/module/CHANGELOG.md) file show the compatibility of this Finnish translation with the different Foundry VTT versions. The goal is to release a translation for each Foundry VTT version after 0.7.5.

## Contributing
See [CONTRIBUTING.md](/CONTRIBUTING.md).

## Screenshots
![Translated actors directory.](/dist/img/actors_directory.png)
![Translated scene configuration window.](/dist/img/scene_configuration.png)
![Translated map note configuration window.](/dist/img/map_note_configuration.png)
