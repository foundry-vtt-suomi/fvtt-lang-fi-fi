/**
 *  Various Handlebars template helpers for convenience.
 */
class LangFiFiHandlebarsHelpers {
    /**
     * Prefixes the module name and a dot to the specified string_id and then calls the default localize helper.
     *
     * @param {String} string_id    The string ID to translate
     * @param {Object} data         Data to pass to localize
     *
     * @return {string}
     * @see HandlebarsHelpers.localize
     */
    static mlocalize(string_id, data) {
        return HandlebarsHelpers.localize(`lang-fi-fi.${string_id}`, data);
    }

    // noinspection JSValidateJSDoc
    /**
     * Insert a dynamic link into localized text.
     *
     * @return {Handlebars.SafeString}
     * @see HandlebarsHelpers.localize
     */
    static llocalize(stringId, data) {
        const esc_url = Handlebars.escapeExpression(game.i18n.localize(data.hash.linkUrl));
        const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.linkTitle));
        const esc_text = Handlebars.escapeExpression(game.i18n.localize(data.hash.linkText));
        const html = `<a href="${esc_url}" title="${esc_title}">${esc_text}</a>`;

        return new Handlebars.SafeString(game.i18n.localize(stringId).replace(new RegExp("{link}", "g"), html));
    }

    // noinspection JSValidateJSDoc
    /**
     * Create a button that resembles text. When clicked it will copy some data into the clipboard.
     *
     * @return {Handlebars.SafeString}
     */
    static clickCopy(display_text, data) {
        const esc_display = Handlebars.escapeExpression(display_text);
        const esc_copy = Handlebars.escapeExpression(data.hash.copy_text);
        const tooltip = Handlebars.escapeExpression(game.i18n.localize("lang-fi-fi.CopyTooltip"));
        const html = `<button class="click-copy-button" data-clipboard-copy-text="${esc_copy}"><span class="click-copy-text">${esc_display || "?"}</span><span class="click-copy-tooltip">${tooltip}</span></button>`;

        return new Handlebars.SafeString(html);
    }

    // noinspection JSValidateJSDoc
    /**
     * Create checked/unchecked checkbox symbol using FontAwesome.
     *
     * @return {Handlebars.SafeString}
     */
    static checkboxSymbol(checked, data) {
        let result;

        if (checked) {
            const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleChecked));
            result = `<i class="far fa-check-square checkbox-symbol" title="${esc_title}"></i>`;
        } else {
            const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleUnchecked));
            result = `<i class="far fa-square checkbox-symbol" title="${esc_title}"></i></span>`;
        }

        return new Handlebars.SafeString(result);
    }

    // noinspection JSValidateJSDoc
    /**
     * Create checked/unchecked radio symbol using FontAwesome.
     *
     * @return {Handlebars.SafeString}
     */
    static radioSymbol(checked, data) {
        let result;

        if (checked) {
            const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleChecked));
            result = `<i class="fas fa-circle checkbox-symbol" title="${esc_title}"></i>`;
        } else {
            const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleUnchecked));
            result = `<i class="far fa-circle checkbox-symbol" title="${esc_title}"></i></span>`;
        }

        return new Handlebars.SafeString(result);
    }

    // noinspection JSValidateJSDoc
    /**
     * Create ok/not ok symbol using FontAwesome.
     *
     * @return {Handlebars.SafeString}
     */
    static checkSymbol(ok, data) {
        let result;
        let enabled = true;

        if (Object.prototype.hasOwnProperty.call(data.hash, "enabled"))
            enabled = data.hash.enabled;

        if (ok) {
            if (enabled) {
                const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleOkEnabled));
                result = `<i class="far fa-check-circle check-symbol" title="${esc_title}"></i>`;
            } else {
                const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleOkDisabled));
                result = `<i class="far fa-minus-circle check-symbol" title="${esc_title}"></i>`;
            }
        } else {
            const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleNotOk));
            result = `<i class="far fa-times-circle check-symbol" title="${esc_title}"></i></span>`;
        }

        return new Handlebars.SafeString(result);
    }
}

class LangFiFiLogging {
    static _isUsingDarkMode() {
        return window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches;
    }

    static _getLogTextColor() {
        if (LangFiFiLogging._isUsingDarkMode())
            return "color:#ffffff";
        else
            return "color:#000000";
    }

    static _prettyLog(log_func, msg, ...args) {
        const text = `%clang-fi-fi%c | ${msg}`;

        if (null === args || 0 === args.length)
            log_func(text, "color:#18447e", LangFiFiLogging._getLogTextColor());
        else
            log_func(text, "color:#18447e", LangFiFiLogging._getLogTextColor(), args);
    }

    static debug(msg, ...args) {
    }

    static log(msg, ...args) {
        LangFiFiLogging._prettyLog(console.log, msg, ...args);
    }

    static warn(msg, ...args) {
        LangFiFiLogging._prettyLog(console.warn, msg, ...args);
    }

    static error(msg, ...args) {
        LangFiFiLogging._prettyLog(console.error, msg, ...args);
    }
}

class LangFiFiDialog extends Dialog {
    /** @override */
    constructor(data = {}, options = {}) {
        super(data, options);
    }

    static async showDialog(langfifi) {
        const data = langfifi.status;

        data.fvtt.version_full = `${game.i18n.localize("Foundry Virtual Tabletop")} ${data.fvtt.version}`;
        data.tinymce.version_full = `TinyMCE ${langfifi.status.tinymce.version}`;
        data.tinymce.translation_version_full = `TinyMCE ${langfifi.status.tinymce.translation_version}`;

        data.module = {
            description: "Suomenkielinen käännös Foundry VTT:n käyttöliittymälle. | Finnish translation for the Foundry VTT user interface.",
            version: "17.0.1",
            version_full: "lang-fi-fi 17.0.1",
            minCoreVersion: "10.291",
            minCoreVersion_full: `${game.i18n.localize("Foundry Virtual Tabletop")} 10.291`,
            maxCoreVersion: "10.312",
            maxCoreVersion_full: `${game.i18n.localize("Foundry Virtual Tabletop")} 10.312`,
            supportMultipleVersions: "10.291" !== "10.312",
            url: "https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/tree/main/dist",
            bugs: "https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/issues",
            // Next line must use single quotes due to string replacement.
            authors: JSON.parse('[{"name":"Demian Wright","discord":"demian.wright"}]'), // jshint ignore:line
            changelog: "https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases"
        };

        const html = await renderTemplate("modules/lang-fi-fi/templates/info.html", data);

        const dialog = new LangFiFiDialog(
            {
                "title": game.i18n.format("lang-fi-fi.InfoDialog.WindowTitle", { module_title: "Suomi [ydin] | Finnish [Core]" }),
                "content": html,
                "buttons": {
                    close: {
                        label: game.i18n.localize("Close")
                    }
                },
                "default": "close"
            });

        dialog.render(true);
    }

    static addClickCopyListeners(event) {
        // TODO: I have a feeling this could be done in a cleaner fashion... but this works.
        const element = event.target.parentElement;
        const tooltip = element.children[1];
        const old_tooltip_text = tooltip.textContent;
        const toggle_attribute_name = "clipboard-copy-toggle";
        const copy_text = element.dataset.clipboardCopyText || element.children[0].textContent;
        const timeout_id = element.getAttribute(toggle_attribute_name);

        const hideTooltip = () => {
            tooltip.innerHTML = old_tooltip_text;
            tooltip.classList.add("hidden");
        };

        const unhideTooltip = () => {
            tooltip.innerHTML = old_tooltip_text;
            tooltip.classList.remove("hidden");
            element.removeAttribute(toggle_attribute_name);
        };

        if (null === timeout_id) {
            element.addEventListener("mouseout", () => {
                clearTimeout(timeout_id);
                unhideTooltip();
            }, {
                once: true,
                passive: true
            });
        } else {
            clearTimeout(timeout_id);
        }

        navigator.clipboard.writeText(copy_text)
            .then(() => {
                unhideTooltip();
                tooltip.innerHTML = game.i18n.localize("lang-fi-fi.CopiedTooltip");
                element.setAttribute(toggle_attribute_name, setTimeout(hideTooltip, 1500));
            }, (err) => {
                console.error("Failed to copy text to clipboard: ", err);
            });
    }

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            id: "lang-fi-fi-info",
            width: "400",
            resizable: true
        });
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // Chrome for one disallows clipboard access without HTTPS.
        if (window.isSecureContext) {
            const copy_buttons = html.find(".click-copy-button");

            for (let i = 0; i < copy_buttons.length; ++i) {
                const button = copy_buttons[i];
                button.classList.add("secure");
                button.addEventListener("click", LangFiFiDialog.addClickCopyListeners, false);
            }
        }
    }
}

class LangFiFiUi {
    constructor(langfifi) {
        this.langfifi = langfifi;
    }

    registerHandlebarsHelpers() {
        const helpers = {
            "mlocalize": LangFiFiHandlebarsHelpers.mlocalize,
            "llocalize": LangFiFiHandlebarsHelpers.llocalize,
            "click-copy": LangFiFiHandlebarsHelpers.clickCopy,
            "checkbox-symbol": LangFiFiHandlebarsHelpers.checkboxSymbol,
            "radio-symbol": LangFiFiHandlebarsHelpers.radioSymbol,
            "check-symbol": LangFiFiHandlebarsHelpers.checkSymbol
        };

        Handlebars.registerHelper(helpers);

        LangFiFiLogging.debug("Handlebars helpers registered:", Object.keys(helpers));
    }

    addMenuButtons(langfifi) {
        Hooks.on("renderSettings", (app, html) => { // jshint ignore:line
            const infoButton = document.createElement("button");
            const buttonText = game.i18n.localize("lang-fi-fi.InfoDialog.ButtonText");
            const buttonId = "lang-fi-fi-info-button";
            infoButton.innerHTML = `<i class="fas fa-info"></i> ${buttonText}`;
            infoButton.id = buttonId;

            html.find("button[data-action='wiki']").after(infoButton);
            html.find(`#${buttonId}`).click((event) => {
                event.stopPropagation();
                LangFiFiDialog.showDialog(langfifi).catch(console.error);
            });
        });

        LangFiFiLogging.debug("Menu button hook added.");
    }

    setup() {
        this.registerHandlebarsHelpers();
        this.addMenuButtons(this.langfifi);

        LangFiFiLogging.debug("UI setup done.");

        return true;
    }
}

class LangFiFi {
    constructor() {
        this.status = {
            translation: {
                ok: false,
                version_ok: false
            },
            progress: {
                ui: false,
                note_icons: false,
                tours: false,
                keyboard: false,
                tinymce: false
            },
            note_icons: {
                ok: false,
                missing_translations: 0
            },
            tours: {
                ok: false,
                missing_translations: 0
            },
            keyboard: {
                ok: false,
                enaled: false
            },
            tinymce: {
                ok: false,
                version: `${tinyMCE.majorVersion}.${tinyMCE.minorVersion}`,
                one_core_installed: false,
                extras_installed: false,
                installed_custom: false,
                installed_plugin: false,
                installed_core_js: false,
                installed_core_json: false
            }
        };
    }

    static getClientLanguage() {
        return game.settings.get("core", "language");
    }

    isClientThisLanguage() {
        return "fi-FI" === LangFiFi.getClientLanguage();
    }

    async localizeNoteIcons(note_icons_translation_path) {
        fetch(note_icons_translation_path)
            .then((response) => {
                if (response.ok)
                    return response.json();

                LangFiFiLogging.error(`Failed to load Note Icons Finnish localization: [${response.status}] ${response.statusText}`);
                return null;
            })
            .then((localized_names) => {
                const localized_note_icons = {};
                const unlocalized_names = [];
                const collator = new Intl.Collator("fi", {
                    numeric: true,
                    sensitivity: "base"
                });

                for (const source_icon_name in CONFIG.JournalEntry.noteIcons) {
                    if (Object.prototype.hasOwnProperty.call(CONFIG.JournalEntry.noteIcons, source_icon_name)) {
                        // Default to unlocalized icon name to ensure newly added unlocalized icons are not skipped.
                        let icon_name = source_icon_name;

                        if (Object.prototype.hasOwnProperty.call(localized_names, source_icon_name))
                            icon_name = localized_names[source_icon_name];
                        else
                            unlocalized_names.push(source_icon_name);

                        localized_note_icons[icon_name] = CONFIG.JournalEntry.noteIcons[source_icon_name];
                    }
                }

                // Sort icons according to the localized names.
                CONFIG.JournalEntry.noteIcons = Object.keys(localized_note_icons)
                    .sort((a, b) => collator.compare(a, b))
                    .reduce((acc, key) => {
                        acc[key] = localized_note_icons[key];
                        return acc;
                    }, {});

                if (0 < unlocalized_names.length) {
                    this.status.note_icons.missing_translations = unlocalized_names.length;
                    LangFiFiLogging.warn(`Missing localization for ${this.status.note_icons.missing_translations} Note Icons: ${unlocalized_names.join(", ")}`);
                }

                LangFiFiLogging.log("Loaded Finnish localization: Note Icons");

                this.status.note_icons.ok = 0 === this.status.note_icons.missing_translations;
                this.status.progress.note_icons = true;
                this.tryCompleteSetup();
            });
    }

    async localizeTours(tours_translation_path) {
        fetch(tours_translation_path)
            .then((response) => {
                if (response.ok)
                    return response.json();

                LangFiFiLogging.error(`Failed to load Tours Finnish localization: [${response.status}] ${response.statusText}`);
                return null;
            })
            .then((localized_tour_keys) => {
                // Check for missing translations.
                // This is only to show stats in the info dialog.
                const unlocalized_tour_keys = [];

                for (const current_tour of game.tours.values()) {
                    for (const tour_localization_key in current_tour.config.localization) {
                        if (Object.prototype.hasOwnProperty.call(current_tour.config.localization, tour_localization_key) && !Object.prototype.hasOwnProperty.call(localized_tour_keys, tour_localization_key))
                            unlocalized_tour_keys.push(tour_localization_key);
                    }
                }

                if (0 < unlocalized_tour_keys.length) {
                    this.status.tours.missing_translations = unlocalized_tour_keys.length;
                    LangFiFiLogging.warn(`Missing localization for ${this.status.tours.missing_translations} Tour keys.`);
                }

                mergeObject(game.i18n.translations, localized_tour_keys);
                LangFiFiLogging.log("Loaded Finnish localization: Tours");

                this.status.tours.ok = 0 === this.status.tours.missing_translations;
                this.status.progress.tours = true;
                this.tryCompleteSetup();
            });
    }

    static getLocalizedKeycodeDisplayString(code) {
        let cleanedCode = code;

        // Default behavior.
        if (cleanedCode in KeyboardManager.KEYCODE_DISPLAY_MAPPING)
            return KeyboardManager.KEYCODE_DISPLAY_MAPPING[cleanedCode];
        // Added behavior for translating more numpad keys.
        else if (code.startsWith("Numpad") && "Numpad" in KeyboardManager.KEYCODE_DISPLAY_MAPPING) {
            cleanedCode = code.replace("Numpad", "");
            return `${KeyboardManager.KEYCODE_DISPLAY_MAPPING.Numpad} ${cleanedCode}`;
        }

        // Default behavior.
        if (code.startsWith("Digit"))
            cleanedCode = code.replace("Digit", "");
        // Default behavior.
        else if (code.startsWith("Key"))
            cleanedCode = code.replace("Key", "");

        // Added behavior for translating more keys overall, if they happen to match one of the translated keys.
        if (cleanedCode in KeyboardManager.KEYCODE_DISPLAY_MAPPING)
            return KeyboardManager.KEYCODE_DISPLAY_MAPPING[cleanedCode];

        return cleanedCode;
    }

    async localizeKeyboard(keyboard_translation_path) {
        if (game.settings.get("lang-fi-fi", "LocalizeAdditionalKeyboardKeys")) {
            fetch(keyboard_translation_path)
                .then((response) => {
                    if (response.ok)
                        return response.json();

                    LangFiFiLogging.error(`Failed to load keyboard Finnish localization: [${response.status}] ${response.statusText}`);
                    return null;
                })
                .then((localized_keyboard_keys) => {
                    // Merge and overwrite the existing keycode display strings.
                    mergeObject(KeyboardManager.KEYCODE_DISPLAY_MAPPING, localized_keyboard_keys);

                    // Replace the default display string function with our own that has additional logic to translate more keys.
                    KeyboardManager.getKeycodeDisplayString = LangFiFi.getLocalizedKeycodeDisplayString;

                    LangFiFiLogging.log("Loaded Finnish localization: Keyboard");

                    this.status.keyboard.ok = true;
                    this.status.keyboard.enabled = true;
                    this.status.progress.keyboard = true;
                    this.tryCompleteSetup();
                });
        } else {
            this.status.keyboard.ok = true;
            this.status.keyboard.enabled = false;
            this.status.progress.keyboard = true;
            this.tryCompleteSetup();
        }
    }

    async _getOfficialTinyMCELocalization(path) {
        // Try to read the official JS localization file.
        return fetch(path)
            .then((response_official) => {
                if (response_official.ok) {
                    return response_official.text()
                        .then((js_text) => {
                            // Get the text between braces.
                            const startSearch = "addI18n(\"fi\",{";
                            const endSearch = "});";
                            let startIdx = js_text.indexOf(startSearch);
                            let endIdx = js_text.lastIndexOf(endSearch);

                            if (-1 < startIdx && -1 < endIdx) {
                                // Include the braces in the substring.
                                startIdx = startIdx + startSearch.length - 1;
                                endIdx += 1;
                                const json = JSON.parse(js_text.substring(startIdx, endIdx).replace(/\\/g, "\\\\"));

                                if (json) {
                                    this.status.tinymce.installed_core_js = true;
                                    // eslint-disable-next-line no-unused-vars
                                    return new Promise((resolve, reject) => resolve(json)); // jshint ignore:line
                                }

                                LangFiFiLogging.error("Found the official core TinyMCE \"Finnish\" JS localization file but failed to parse JSON from it. The formatting of the file has likely changed, please inform the module maintainers.");
                                return null;
                            }

                            LangFiFiLogging.error("Found the official core TinyMCE \"Finnish\" JS localization file but failed to parse it at all. The formatting of the file has likely changed, please inform the module maintainers.");
                            return null;
                        });
                }

                // Didn't find official JS localization, try to read a custom JSON localization.
                return fetch("modules/lang-fi-fi/lang/tinymce/fi-FI.json")
                    .then((response_custom) => {
                        if (response_custom.ok) {
                            this.status.tinymce.installed_core_json = true;
                            return response_custom.json();
                        }

                        LangFiFiLogging.error(`Failed to load TinyMCE "Finnish" localization JS or JSON (official): [${response_custom.status}] ${response_custom.statusText}`);
                        return null;
                    });
            });
    }

    async _getTinyMCEFVTTLocalization(path) {
        return fetch(path)
            .then((response) => {
                if (response.ok)
                    return response.json();

                LangFiFiLogging.error(`Failed to load TinyMCE "Finnish" localization (Foundry VTT additions): [${response.status}] ${response.statusText}`);
                return null;
            });
    }

    async localizeTinyMCE(tinymce_translation_path) {
        this._getTinyMCEFVTTLocalization(`${tinymce_translation_path}/fi-FI_custom.json`)
            .then((custom_fvtt_json) => {
                custom_fvtt_json = custom_fvtt_json ? custom_fvtt_json : {};

                this._getOfficialTinyMCELocalization(`${tinymce_translation_path}/fi.js`)
                    .then((official_json) => {
                        const has_custom_json = {} !== custom_fvtt_json;
                        let has_official_json = true;

                        if (!official_json) {
                            official_json = {};
                            has_official_json = false;
                        }

                        // Combine custom menu localization with the official localization.
                        // Since 10.282 the mergeObject function cannot handle keys with multiple periods in it, e.g. "Anchors..." as it apparently tries to assign data into empty string "" members.
                        const localization = Object.assign(official_json, custom_fvtt_json);

                        if (localization) {
                            tinyMCE.addI18n("fi", localization);
                            let type_message = "";

                            if (has_custom_json && !has_official_json)
                                type_message = " (Extra menus only)";
                            else if (!has_custom_json && has_official_json)
                                type_message = " (Official only)";

                            LangFiFiLogging.log(`Loaded Finnish localization${type_message}: TinyMCE (${this.status.tinymce.version})`);

                            if (custom_fvtt_json) {
                                this.status.tinymce.installed_custom = true;
                                this.status.tinymce.installed_plugin = true;
                                this.status.tinymce.extras_installed = this.status.tinymce.installed_custom && this.status.tinymce.installed_plugin;
                            }
                        }
                    })
                    // eslint-disable-next-line no-unused-vars
                    .then((response) => { // jshint ignore:line
                        this.status.tinymce.translation_version = "6.1.2";
                        this.status.tinymce.translation_not_outdated = !isNewerVersion(this.status.tinymce.version, this.status.tinymce.translation_version);
                        this.status.tinymce.one_core_installed = this.status.tinymce.installed_core_js || this.status.tinymce.installed_core_json && !(this.status.tinymce.installed_core_js && this.status.tinymce.installed_core_json);

                        if (!this.status.tinymce.translation_not_outdated)
                            LangFiFiLogging.warn(`TinyMCE translation (${this.status.tinymce.translation_version}) is outdated! Current version: ${this.status.tinymce.version}`);

                        CONFIG.TinyMCE = mergeObject(CONFIG.TinyMCE, {
                            language: "fi"
                        });

                        this.status.tinymce.ok = this.status.tinymce.translation_not_outdated && this.status.tinymce.one_core_installed && this.status.tinymce.extras_installed;
                        this.status.progress.tinymce = true;

                        this.tryCompleteSetup();
                    });
            });
    }

    setupUi() {
        this.status.progress.ui = new LangFiFiUi(this).setup();
        this.tryCompleteSetup();
    }

    tryCompleteSetup() {
        if (Object.values(this.status.progress).every((val) => true === val)) {
            this.status.translation.ok = this.status.translation.version_ok && this.status.note_icons.ok && this.status.tours.ok;

            LangFiFiLogging.debug("Setup done.");
        }
    }

    onceSetup() {
        // Only localize if the user has selected this language.
        if (this.isClientThisLanguage()) {
            this.status.fvtt = {
                version: `${game.release.generation}.${game.release.build}`
            };

            this.status.translation.version_ok = !isNewerVersion(this.status.fvtt.version, "10.312");

            this.localizeTinyMCE("modules/lang-fi-fi/lang/tinymce").catch(console.error);
            this.localizeNoteIcons("modules/lang-fi-fi/lang/fvtt/fi-FI_icons.json").catch(console.error);
            this.localizeTours("modules/lang-fi-fi/lang/fvtt/fi-FI_tours.json").catch(console.error);
            this.localizeKeyboard("modules/lang-fi-fi/lang/fvtt/fi-FI_keyboard.json").catch(console.error);
            this.setupUi();
        } else 
            LangFiFiLogging.debug(`Not doing setup: client language is '${LangFiFi.getClientLanguage()}' instead of 'fi-FI'.`);
    }

    registerSettings() {
        game.settings.register("lang-fi-fi", "LocalizeAdditionalKeyboardKeys", {
            "name": "lang-fi-fi.Settings.LocalizeAdditionalKeyboardKeysLabel",
            "hint": "lang-fi-fi.Settings.LocalizeAdditionalKeyboardKeysHint",
            "default": true,
            "type": Boolean,
            "scope": "client",
            "config": true,
            "requiresReload": true
        });

        LangFiFiLogging.debug("Settings registered.");
    }
}

const langfifi = new LangFiFi();

Hooks.once("init", () => langfifi.registerSettings());
Hooks.once("setup", () => langfifi.onceSetup());

export { langfifi };
