import "./sass/info-dialog.scss";
import "./sass/tooltip.scss";
import "./sass/translation.scss";

import LangFiFi from "./modules/module.js";

export const langfifi = new LangFiFi();

Hooks.once("init", () => langfifi.registerSettings());
Hooks.once("setup", () => langfifi.onceSetup());