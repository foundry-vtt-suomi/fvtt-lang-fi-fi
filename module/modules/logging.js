import { IS_DEV_BUILD } from "./const.js";

export class LangFiFiLogging {
    static _isUsingDarkMode() {
        return window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches;
    }

    static _getLogTextColor() {
        if (LangFiFiLogging._isUsingDarkMode())
            return "color:#ffffff";
        else
            return "color:#000000";
    }

    static _prettyLog(log_func, msg, ...args) {
        const text = `%c__MODULE_ID__%c | ${msg}`;

        if (null === args || 0 === args.length)
            log_func(text, "color:__MODULE_COLOR__", LangFiFiLogging._getLogTextColor());
        else
            log_func(text, "color:__MODULE_COLOR__", LangFiFiLogging._getLogTextColor(), args);
    }

    static debug(msg, ...args) {
        if (IS_DEV_BUILD)
            LangFiFiLogging._prettyLog(console.debug, msg, ...args);
    }

    static log(msg, ...args) {
        LangFiFiLogging._prettyLog(console.log, msg, ...args);
    }

    static warn(msg, ...args) {
        LangFiFiLogging._prettyLog(console.warn, msg, ...args);
    }

    static error(msg, ...args) {
        LangFiFiLogging._prettyLog(console.error, msg, ...args);
    }
}
