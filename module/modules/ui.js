import LangFiFiHandlebarsHelpers from "./handlebars_helpers.js";
import { LangFiFiLogging as logging } from "./logging.js";
import { IS_DEV_BUILD } from "./const.js";

export class LangFiFiDialog extends Dialog {
    /** @override */
    constructor(data = {}, options = {}) {
        super(data, options);
    }

    static async showDialog(langfifi) {
        const data = langfifi.status;

        data.fvtt.version_full = `${game.i18n.localize("Foundry Virtual Tabletop")} ${data.fvtt.version}`;
        data.tinymce.version_full = `TinyMCE ${langfifi.status.tinymce.version}`;
        data.tinymce.translation_version_full = `TinyMCE ${langfifi.status.tinymce.translation_version}`;

        data.module = {
            description: "__MODULE_DESCRIPTION__",
            version: "__MODULE_VERSION__",
            version_full: "__MODULE_ID__ __MODULE_VERSION__",
            minCoreVersion: "__MINIMUM_CORE_VERSION__",
            minCoreVersion_full: `${game.i18n.localize("Foundry Virtual Tabletop")} __MINIMUM_CORE_VERSION__`,
            maxCoreVersion: "__VERIFIED_CORE_VERSION__",
            maxCoreVersion_full: `${game.i18n.localize("Foundry Virtual Tabletop")} __VERIFIED_CORE_VERSION__`,
            supportMultipleVersions: "__MINIMUM_CORE_VERSION__" !== "__VERIFIED_CORE_VERSION__",
            url: "__REPO_DIST_URL__",
            bugs: "__REPO_ISSUES_URL__",
            // Next line must use single quotes due to string replacement.
            authors: JSON.parse('__MODULE_AUTHORS__'), // jshint ignore:line
            changelog: "__REPO_RELEASES_URL__"
        };

        const html = await renderTemplate("__MODULE_PATH__/templates/info.html", data);

        const dialog = new LangFiFiDialog(
            {
                "title": game.i18n.format("__MODULE_ID__.InfoDialog.WindowTitle", { module_title: "__MODULE_TITLE__" }),
                "content": html,
                "buttons": {
                    close: {
                        label: game.i18n.localize("Close")
                    }
                },
                "default": "close"
            });

        dialog.render(true);
    }

    static addClickCopyListeners(event) {
        // TODO: I have a feeling this could be done in a cleaner fashion... but this works.
        const element = event.target.parentElement;
        const tooltip = element.children[1];
        const old_tooltip_text = tooltip.textContent;
        const toggle_attribute_name = "clipboard-copy-toggle";
        const copy_text = element.dataset.clipboardCopyText || element.children[0].textContent;
        const timeout_id = element.getAttribute(toggle_attribute_name);

        const hideTooltip = () => {
            tooltip.innerHTML = old_tooltip_text;
            tooltip.classList.add("hidden");
        };

        const unhideTooltip = () => {
            tooltip.innerHTML = old_tooltip_text;
            tooltip.classList.remove("hidden");
            element.removeAttribute(toggle_attribute_name);
        };

        if (null === timeout_id) {
            element.addEventListener("mouseout", () => {
                clearTimeout(timeout_id);
                unhideTooltip();
            }, {
                once: true,
                passive: true
            });
        } else {
            clearTimeout(timeout_id);
        }

        navigator.clipboard.writeText(copy_text)
            .then(() => {
                unhideTooltip();
                tooltip.innerHTML = game.i18n.localize("__MODULE_ID__.CopiedTooltip");
                element.setAttribute(toggle_attribute_name, setTimeout(hideTooltip, 1500));
            }, (err) => {
                console.error("Failed to copy text to clipboard: ", err);
            });
    }

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            id: "__MODULE_ID__-info",
            width: "400",
            resizable: true
        });
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // Chrome for one disallows clipboard access without HTTPS.
        if (window.isSecureContext) {
            const copy_buttons = html.find(".click-copy-button");

            for (let i = 0; i < copy_buttons.length; ++i) {
                const button = copy_buttons[i];
                button.classList.add("secure");
                button.addEventListener("click", LangFiFiDialog.addClickCopyListeners, false);
            }
        }
    }
}

export class LangFiFiUi {
    constructor(langfifi) {
        this.langfifi = langfifi;
    }

    registerHandlebarsHelpers() {
        "use strict";
        const helpers = {
            "mlocalize": LangFiFiHandlebarsHelpers.mlocalize,
            "llocalize": LangFiFiHandlebarsHelpers.llocalize,
            "click-copy": LangFiFiHandlebarsHelpers.clickCopy,
            "checkbox-symbol": LangFiFiHandlebarsHelpers.checkboxSymbol,
            "radio-symbol": LangFiFiHandlebarsHelpers.radioSymbol,
            "check-symbol": LangFiFiHandlebarsHelpers.checkSymbol
        };

        Handlebars.registerHelper(helpers);

        logging.debug("Handlebars helpers registered:", Object.keys(helpers));
    }

    addMenuButtons(langfifi) {
        "use strict";
        Hooks.on("renderSettings", (app, html) => { // jshint ignore:line
            const infoButton = document.createElement("button");
            const buttonText = game.i18n.localize("__MODULE_ID__.InfoDialog.ButtonText");
            const buttonId = "__MODULE_ID__-info-button";
            infoButton.innerHTML = `<i class="fas fa-info"></i> ${buttonText}`;
            infoButton.id = buttonId;

            html.find("button[data-action='wiki']").after(infoButton);
            html.find(`#${buttonId}`).click((event) => {
                event.stopPropagation();
                LangFiFiDialog.showDialog(langfifi).catch(console.error);
            });
        });

        logging.debug("Menu button hook added.");
    }

    setup() {
        this.registerHandlebarsHelpers();
        this.addMenuButtons(this.langfifi);

        logging.debug("UI setup done.");

        return true;
    }
}
