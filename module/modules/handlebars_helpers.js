/**
 *  Various Handlebars template helpers for convenience.
 */
export default class LangFiFiHandlebarsHelpers {
    /**
     * Prefixes the module name and a dot to the specified string_id and then calls the default localize helper.
     *
     * @param {String} string_id    The string ID to translate
     * @param {Object} data         Data to pass to localize
     *
     * @return {string}
     * @see HandlebarsHelpers.localize
     */
    static mlocalize(string_id, data) {
        return HandlebarsHelpers.localize(`__MODULE_ID__.${string_id}`, data);
    }

    // noinspection JSValidateJSDoc
    /**
     * Insert a dynamic link into localized text.
     *
     * @return {Handlebars.SafeString}
     * @see HandlebarsHelpers.localize
     */
    static llocalize(stringId, data) {
        const esc_url = Handlebars.escapeExpression(game.i18n.localize(data.hash.linkUrl));
        const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.linkTitle));
        const esc_text = Handlebars.escapeExpression(game.i18n.localize(data.hash.linkText));
        const html = `<a href="${esc_url}" title="${esc_title}">${esc_text}</a>`;

        return new Handlebars.SafeString(game.i18n.localize(stringId).replace(new RegExp("{link}", "g"), html));
    }

    // noinspection JSValidateJSDoc
    /**
     * Create a button that resembles text. When clicked it will copy some data into the clipboard.
     *
     * @return {Handlebars.SafeString}
     */
    static clickCopy(display_text, data) {
        const esc_display = Handlebars.escapeExpression(display_text);
        const esc_copy = Handlebars.escapeExpression(data.hash.copy_text);
        const tooltip = Handlebars.escapeExpression(game.i18n.localize("__MODULE_ID__.CopyTooltip"));
        const html = `<button class="click-copy-button" data-clipboard-copy-text="${esc_copy}"><span class="click-copy-text">${esc_display || "?"}</span><span class="click-copy-tooltip">${tooltip}</span></button>`;

        return new Handlebars.SafeString(html);
    }

    // noinspection JSValidateJSDoc
    /**
     * Create checked/unchecked checkbox symbol using FontAwesome.
     *
     * @return {Handlebars.SafeString}
     */
    static checkboxSymbol(checked, data) {
        let result;

        if (checked) {
            const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleChecked));
            result = `<i class="far fa-check-square checkbox-symbol" title="${esc_title}"></i>`;
        } else {
            const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleUnchecked));
            result = `<i class="far fa-square checkbox-symbol" title="${esc_title}"></i></span>`;
        }

        return new Handlebars.SafeString(result);
    }

    // noinspection JSValidateJSDoc
    /**
     * Create checked/unchecked radio symbol using FontAwesome.
     *
     * @return {Handlebars.SafeString}
     */
    static radioSymbol(checked, data) {
        let result;

        if (checked) {
            const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleChecked));
            result = `<i class="fas fa-circle checkbox-symbol" title="${esc_title}"></i>`;
        } else {
            const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleUnchecked));
            result = `<i class="far fa-circle checkbox-symbol" title="${esc_title}"></i></span>`;
        }

        return new Handlebars.SafeString(result);
    }

    // noinspection JSValidateJSDoc
    /**
     * Create ok/not ok symbol using FontAwesome.
     *
     * @return {Handlebars.SafeString}
     */
    static checkSymbol(ok, data) {
        let result;
        let enabled = true;

        if (Object.prototype.hasOwnProperty.call(data.hash, "enabled"))
            enabled = data.hash.enabled;

        if (ok) {
            if (enabled) {
                const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleOkEnabled));
                result = `<i class="far fa-check-circle check-symbol" title="${esc_title}"></i>`;
            } else {
                const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleOkDisabled));
                result = `<i class="far fa-minus-circle check-symbol" title="${esc_title}"></i>`;
            }
        } else {
            const esc_title = Handlebars.escapeExpression(game.i18n.localize(data.hash.titleNotOk));
            result = `<i class="far fa-times-circle check-symbol" title="${esc_title}"></i></span>`;
        }

        return new Handlebars.SafeString(result);
    }
}
