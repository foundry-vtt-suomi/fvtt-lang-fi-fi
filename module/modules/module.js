import { LangFiFiDialog, LangFiFiUi } from "./ui.js";
import { LangFiFiLogging as logging } from "./logging.js";
import { IS_DEV_BUILD } from "./const.js";

export default class LangFiFi {
    constructor() {
        this.status = {
            translation: {
                ok: false,
                version_ok: false
            },
            progress: {
                ui: false,
                note_icons: false,
                tours: false,
                keyboard: false,
                tinymce: false
            },
            note_icons: {
                ok: false,
                missing_translations: 0
            },
            tours: {
                ok: false,
                missing_translations: 0
            },
            keyboard: {
                ok: false,
                enaled: false
            },
            tinymce: {
                ok: false,
                version: `${tinyMCE.majorVersion}.${tinyMCE.minorVersion}`,
                one_core_installed: false,
                extras_installed: false,
                installed_custom: false,
                installed_plugin: false,
                installed_core_js: false,
                installed_core_json: false
            }
        };
    }

    static getClientLanguage() {
        return game.settings.get("core", "language");
    }

    isClientThisLanguage() {
        "use strict";
        return "__LANG_TAG_CANONICAL__" === LangFiFi.getClientLanguage();
    }

    async localizeNoteIcons(note_icons_translation_path) {
        "use strict";
        fetch(note_icons_translation_path)
            .then((response) => {
                if (response.ok)
                    return response.json();

                logging.error(`Failed to load Note Icons __LANG_NAME_EN__ localization: [${response.status}] ${response.statusText}`);
                return null;
            })
            .then((localized_names) => {
                const localized_note_icons = {};
                const unlocalized_names = [];
                const collator = new Intl.Collator("__LANG_TAG_SHORT__", {
                    numeric: true,
                    sensitivity: "base"
                });

                for (const source_icon_name in CONFIG.JournalEntry.noteIcons) {
                    if (Object.prototype.hasOwnProperty.call(CONFIG.JournalEntry.noteIcons, source_icon_name)) {
                        // Default to unlocalized icon name to ensure newly added unlocalized icons are not skipped.
                        let icon_name = source_icon_name;

                        if (Object.prototype.hasOwnProperty.call(localized_names, source_icon_name))
                            icon_name = localized_names[source_icon_name];
                        else
                            unlocalized_names.push(source_icon_name);

                        localized_note_icons[icon_name] = CONFIG.JournalEntry.noteIcons[source_icon_name];
                    }
                }

                // Sort icons according to the localized names.
                CONFIG.JournalEntry.noteIcons = Object.keys(localized_note_icons)
                    .sort((a, b) => collator.compare(a, b))
                    .reduce((acc, key) => {
                        acc[key] = localized_note_icons[key];
                        return acc;
                    }, {});

                if (0 < unlocalized_names.length) {
                    this.status.note_icons.missing_translations = unlocalized_names.length;
                    logging.warn(`Missing localization for ${this.status.note_icons.missing_translations} Note Icons: ${unlocalized_names.join(", ")}`);
                }

                logging.log("Loaded __LANG_NAME_EN__ localization: Note Icons");

                this.status.note_icons.ok = 0 === this.status.note_icons.missing_translations;
                this.status.progress.note_icons = true;
                this.tryCompleteSetup();
            });
    }

    async localizeTours(tours_translation_path) {
        "use strict";
        fetch(tours_translation_path)
            .then((response) => {
                if (response.ok)
                    return response.json();

                logging.error(`Failed to load Tours __LANG_NAME_EN__ localization: [${response.status}] ${response.statusText}`);
                return null;
            })
            .then((localized_tour_keys) => {
                // Check for missing translations.
                // This is only to show stats in the info dialog.
                const unlocalized_tour_keys = [];

                for (const current_tour of game.tours.values()) {
                    for (const tour_localization_key in current_tour.config.localization) {
                        if (Object.prototype.hasOwnProperty.call(current_tour.config.localization, tour_localization_key) && !Object.prototype.hasOwnProperty.call(localized_tour_keys, tour_localization_key))
                            unlocalized_tour_keys.push(tour_localization_key);
                    }
                }

                if (0 < unlocalized_tour_keys.length) {
                    this.status.tours.missing_translations = unlocalized_tour_keys.length;
                    logging.warn(`Missing localization for ${this.status.tours.missing_translations} Tour keys.`);
                }

                mergeObject(game.i18n.translations, localized_tour_keys);
                logging.log("Loaded __LANG_NAME_EN__ localization: Tours");

                this.status.tours.ok = 0 === this.status.tours.missing_translations;
                this.status.progress.tours = true;
                this.tryCompleteSetup();
            });
    }

    static getLocalizedKeycodeDisplayString(code) {
        "use strict";
        let cleanedCode = code;

        // Default behavior.
        if (cleanedCode in KeyboardManager.KEYCODE_DISPLAY_MAPPING)
            return KeyboardManager.KEYCODE_DISPLAY_MAPPING[cleanedCode];
        // Added behavior for translating more numpad keys.
        else if (code.startsWith("Numpad") && "Numpad" in KeyboardManager.KEYCODE_DISPLAY_MAPPING) {
            cleanedCode = code.replace("Numpad", "");
            return `${KeyboardManager.KEYCODE_DISPLAY_MAPPING.Numpad} ${cleanedCode}`;
        }

        // Default behavior.
        if (code.startsWith("Digit"))
            cleanedCode = code.replace("Digit", "");
        // Default behavior.
        else if (code.startsWith("Key"))
            cleanedCode = code.replace("Key", "");

        // Added behavior for translating more keys overall, if they happen to match one of the translated keys.
        if (cleanedCode in KeyboardManager.KEYCODE_DISPLAY_MAPPING)
            return KeyboardManager.KEYCODE_DISPLAY_MAPPING[cleanedCode];

        return cleanedCode;
    }

    async localizeKeyboard(keyboard_translation_path) {
        "use strict";
        if (game.settings.get("__MODULE_ID__", "LocalizeAdditionalKeyboardKeys")) {
            fetch(keyboard_translation_path)
                .then((response) => {
                    if (response.ok)
                        return response.json();

                    logging.error(`Failed to load keyboard __LANG_NAME_EN__ localization: [${response.status}] ${response.statusText}`);
                    return null;
                })
                .then((localized_keyboard_keys) => {
                    // Merge and overwrite the existing keycode display strings.
                    mergeObject(KeyboardManager.KEYCODE_DISPLAY_MAPPING, localized_keyboard_keys);

                    // Replace the default display string function with our own that has additional logic to translate more keys.
                    KeyboardManager.getKeycodeDisplayString = LangFiFi.getLocalizedKeycodeDisplayString;

                    logging.log("Loaded __LANG_NAME_EN__ localization: Keyboard");

                    this.status.keyboard.ok = true;
                    this.status.keyboard.enabled = true;
                    this.status.progress.keyboard = true;
                    this.tryCompleteSetup();
                });
        } else {
            this.status.keyboard.ok = true;
            this.status.keyboard.enabled = false;
            this.status.progress.keyboard = true;
            this.tryCompleteSetup();
        }
    }

    async _getOfficialTinyMCELocalization(path) {
        "use strict";
        // Try to read the official JS localization file.
        return fetch(path)
            .then((response_official) => {
                if (response_official.ok) {
                    return response_official.text()
                        .then((js_text) => {
                            // Get the text between braces.
                            const startSearch = "addI18n(\"__LANG_TAG_TINYMCE__\",{";
                            const endSearch = "});";
                            let startIdx = js_text.indexOf(startSearch);
                            let endIdx = js_text.lastIndexOf(endSearch);

                            if (-1 < startIdx && -1 < endIdx) {
                                // Include the braces in the substring.
                                startIdx = startIdx + startSearch.length - 1;
                                endIdx += 1;
                                const json = JSON.parse(js_text.substring(startIdx, endIdx).replace(/\\/g, "\\\\"));

                                if (json) {
                                    this.status.tinymce.installed_core_js = true;
                                    // eslint-disable-next-line no-unused-vars
                                    return new Promise((resolve, reject) => resolve(json)); // jshint ignore:line
                                }

                                logging.error("Found the official core TinyMCE \"__LANG_NAME_EN__\" JS localization file but failed to parse JSON from it. The formatting of the file has likely changed, please inform the module maintainers.");
                                return null;
                            }

                            logging.error("Found the official core TinyMCE \"__LANG_NAME_EN__\" JS localization file but failed to parse it at all. The formatting of the file has likely changed, please inform the module maintainers.");
                            return null;
                        });
                }

                // Didn't find official JS localization, try to read a custom JSON localization.
                return fetch("__TINYMCE_LANG_ROOT__/__LANG_TAG_CANONICAL__.json")
                    .then((response_custom) => {
                        if (response_custom.ok) {
                            this.status.tinymce.installed_core_json = true;
                            return response_custom.json();
                        }

                        logging.error(`Failed to load TinyMCE "__LANG_NAME_EN__" localization JS or JSON (official): [${response_custom.status}] ${response_custom.statusText}`);
                        return null;
                    });
            });
    }

    async _getTinyMCEFVTTLocalization(path) {
        return fetch(path)
            .then((response) => {
                if (response.ok)
                    return response.json();

                logging.error(`Failed to load TinyMCE "__LANG_NAME_EN__" localization (Foundry VTT additions): [${response.status}] ${response.statusText}`);
                return null;
            });
    }

    async localizeTinyMCE(tinymce_translation_path) {
        "use strict";
        this._getTinyMCEFVTTLocalization(`${tinymce_translation_path}/__LANG_TAG_CANONICAL___custom.json`)
            .then((custom_fvtt_json) => {
                custom_fvtt_json = custom_fvtt_json ? custom_fvtt_json : {};

                this._getOfficialTinyMCELocalization(`${tinymce_translation_path}/__LANG_TAG_TINYMCE__.js`)
                    .then((official_json) => {
                        const has_custom_json = {} !== custom_fvtt_json;
                        let has_official_json = true;

                        if (!official_json) {
                            official_json = {};
                            has_official_json = false;
                        }

                        // Combine custom menu localization with the official localization.
                        // Since 10.282 the mergeObject function cannot handle keys with multiple periods in it, e.g. "Anchors..." as it apparently tries to assign data into empty string "" members.
                        const localization = Object.assign(official_json, custom_fvtt_json);

                        if (localization) {
                            tinyMCE.addI18n("__LANG_TAG_TINYMCE__", localization);
                            let type_message = "";

                            if (has_custom_json && !has_official_json)
                                type_message = " (Extra menus only)";
                            else if (!has_custom_json && has_official_json)
                                type_message = " (Official only)";

                            logging.log(`Loaded Finnish localization${type_message}: TinyMCE (${this.status.tinymce.version})`);

                            if (custom_fvtt_json) {
                                this.status.tinymce.installed_custom = true;
                                this.status.tinymce.installed_plugin = true;
                                this.status.tinymce.extras_installed = this.status.tinymce.installed_custom && this.status.tinymce.installed_plugin;
                            }
                        }
                    })
                    // eslint-disable-next-line no-unused-vars
                    .then((response) => { // jshint ignore:line
                        this.status.tinymce.translation_version = "__TINYMCE_LANG_VERSION__";
                        this.status.tinymce.translation_not_outdated = !isNewerVersion(this.status.tinymce.version, this.status.tinymce.translation_version);
                        this.status.tinymce.one_core_installed = this.status.tinymce.installed_core_js || this.status.tinymce.installed_core_json && !(this.status.tinymce.installed_core_js && this.status.tinymce.installed_core_json);

                        if (!this.status.tinymce.translation_not_outdated)
                            logging.warn(`TinyMCE translation (${this.status.tinymce.translation_version}) is outdated! Current version: ${this.status.tinymce.version}`);

                        CONFIG.TinyMCE = mergeObject(CONFIG.TinyMCE, {
                            language: "__LANG_TAG_TINYMCE__"
                        });

                        this.status.tinymce.ok = this.status.tinymce.translation_not_outdated && this.status.tinymce.one_core_installed && this.status.tinymce.extras_installed;
                        this.status.progress.tinymce = true;

                        this.tryCompleteSetup();
                    });
            });
    }

    setupUi() {
        this.status.progress.ui = new LangFiFiUi(this).setup();
        this.tryCompleteSetup();
    }

    tryCompleteSetup() {
        if (Object.values(this.status.progress).every((val) => true === val)) {
            this.status.translation.ok = this.status.translation.version_ok && this.status.note_icons.ok && this.status.tours.ok;

            logging.debug("Setup done.");

            if (IS_DEV_BUILD)
                LangFiFiDialog.showDialog(this).catch(console.error);
        }
    }

    onceSetup() {
        "use strict";
        // Only localize if the user has selected this language.
        if (this.isClientThisLanguage()) {
            this.status.fvtt = {
                version: `${game.release.generation}.${game.release.build}`
            };

            this.status.translation.version_ok = !isNewerVersion(this.status.fvtt.version, "__VERIFIED_CORE_VERSION__");

            this.localizeTinyMCE("__TINYMCE_LANG_ROOT__").catch(console.error);
            this.localizeNoteIcons("__MODULE_PATH__/lang/fvtt/__LANG_TAG_CANONICAL___icons.json").catch(console.error);
            this.localizeTours("__MODULE_PATH__/lang/fvtt/__LANG_TAG_CANONICAL___tours.json").catch(console.error);
            this.localizeKeyboard("__MODULE_PATH__/lang/fvtt/__LANG_TAG_CANONICAL___keyboard.json").catch(console.error);
            this.setupUi();
        } else 
            logging.debug(`Not doing setup: client language is '${LangFiFi.getClientLanguage()}' instead of '__LANG_TAG_CANONICAL__'.`);
    }

    registerSettings() {
        game.settings.register("__MODULE_ID__", "LocalizeAdditionalKeyboardKeys", {
            "name": "__MODULE_ID__.Settings.LocalizeAdditionalKeyboardKeysLabel",
            "hint": "__MODULE_ID__.Settings.LocalizeAdditionalKeyboardKeysHint",
            "default": true,
            "type": Boolean,
            "scope": "client",
            "config": true,
            "requiresReload": true
        });

        logging.debug("Settings registered.");
    }
}
