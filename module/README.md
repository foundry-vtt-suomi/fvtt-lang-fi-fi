<!--suppress HtmlDeprecatedAttribute, HtmlUnknownTarget -->
# Foundry VTT suomenkielinen käännös (Finnish Translation)
[![FVTT Version](https://img.shields.io/badge/dynamic/json.svg?url=__REPO_RELEASE_MANIFEST_URL_URLENCODED__&label=FVTT%20Version&query=$.compatibility.verified&colorB=orange)](https://foundryvtt.com/ "Foundry Virtual Tabletop verkkosivu") [![Module Version](https://img.shields.io/badge/dynamic/json.svg?url=__REPO_RELEASE_MANIFEST_URL_URLENCODED__&label=Module%20Version&query=$.version&color=orange)](__REPO_MAIN_MANIFEST_URL__ "Manifestin osoite") [![Manifest+ Version](https://img.shields.io/badge/dynamic/json.svg?url=__REPO_RELEASE_MANIFEST_URL_URLENCODED__&label=Manifest%2B%20Version&query=$.manifestPlusVersion&colorB=blue)](https://foundryvtt.wiki/en/development/manifest-plus "Manifest+ spesifikaatio")
[![MIT License](https://img.shields.io/badge/License-MIT-lightgray)](https://opensource.org/licenses/MIT "MIT License") [![__DISCORD_SERVER_NAME__ Discord Invite](https://img.shields.io/discord/__DISCORD_SERVER_ID__?color=%237289DA&label=Chat&logo=discord&logoColor=white)](__DISCORD_INVITE_LINK__ "__DISCORD_SERVER_NAME__ Discord-kutsu")

Suomenkielinen käännös [Foundry Virtual Tabletop](https://foundryvtt.com/ "Foundry Virtual Tabletop verkkosivu.")&nbsp;-&NoBreak;sovelluksen käyttöliittymälle, karttamuistiinpanojen kuvakkeille, TinyMCE-tekstinkäsittelyohjelmalle, tutustumiskierroksille, sekä näppäimistön näppäimille. Lataa uusimman version arkisto **[__MODULE_ID__.zip](__REPO_RELEASE_DOWNLOAD_URL__ "Moduulin ZIP-arkiston suora latauslinkki.")** tai asenna suoraan sovelluksessa manifestin osoitteella:
```
__REPO_MAIN_MANIFEST_URL__
```
Katso [tarkemmat asennusohjeet](#nopea-asennusohje) alta (mm. TinyMCE käännöstiedoston lataaminen ja päivämäärävalitsimen käännös).

Finnish translation for the [Foundry Virtual Tabletop](https://foundryvtt.com/ "Foundry Virtual Tabletop website.") application user interface, map note icons, TinyMCE text editor, tours, and keyboard keys. Download the newest version's archive **[__MODULE_ID__.zip](__REPO_RELEASE_DOWNLOAD_URL__ "Direct download link for the module's ZIP archive.")** or install it directly in the application using the manifest URL above. See [detailed installation instructions](#quick-installation-guide) below (including how to install the TinyMCE translation and translate the date picker). [English README](#in-english-englanniksi) at the end.

![Käännetty ylläpitovalikko.](/dist/img/setup_menu.png)

# Suomeksi (In Finnish)
<img src="/dist/img/module_information_window.png" alt="Käännöksen tietoikkuna." align="right">

Huomaa, että Foundry VTT *ei vielä* tue kaiken tekstin kääntämistä käyttöliittymässä. Kääntämätön teksti näkyy englanniksi. Jos peli on päivittynyt viimeaikoina, annathan kääntäjille aikaa päivittää käännös. Voit katsoa mikä on viimeisin asentamasi käännöksen tukema Foundry VTT:n versio painamalle **Käännöksen tiedot**&nbsp;-&NoBreak;nappia sivupalkin **Peliasetukset**-välilehdeltä, mikäli moduuli on laitettu päälle. Tämä moduuli **ei sisällä** käännöksiä muille paketeille: moduuleille, pelijärjestelmille tai maailmoille.

Yhteisön tekemiä suomenkielisiä käännöksiä moduuleille, pelijärjestelmille ja maailmoille työstetään pääasiassa [Foundry Hub Weblate](https://weblate.foundryvtt-hub.com/languages/__LANG_TAG_SHORT__/) -palvelussa. Joitain yksittäisiä käännöksiä voi olla **Foundry VTT Suomi**&nbsp;-&NoBreak;projektissa [GitLabissa](https://gitlab.com/foundry-vtt-suomi "Foundry VTT Suomi GitLabissa.") ja [GitHubissa](https://github.com/foundry-vtt-suomi "Foundry VTT Suomi GitHubissa"). Ota yhteyttä kääntäjiin, anna palautetta käännöksestä, tai ilmoita käännöksen ongelmista [__DISCORD_SERVER_NAME__&nbsp;-&NoBreak;Discord-palvelimella](__DISCORD_INVITE_LINK__ "__DISCORD_SERVER_NAME__ -Discord-palvelimen kutsulinkki.") Voit myös ilmoittaa ongelmista tämän käännöksen kanssa koodisäilön [ongelmanseuraimessa](__REPO_ISSUES_URL__).

Katso myös suomenkielinen versio [Foundry VTT yhteisöwikistä](https://foundryvtt.wiki/__LANG_TAG_SHORT__/home "Suomenkielinen Foundry VTT yhteisöwiki").

## Ominaisuudet
- Käyttöliittymän suomenkielinen käännös. (100% tekstistä, jonka *voi* kääntää, katso koodisäilön [ongelmanseurain](__REPO_ISSUES_URL__).)
  - Päivämäärävalitsimen kielen vaihtaminen vaatii pelin pikakuvakkeen muokkausta. ([Katso ohjeet alla.](#1-vaihda-päivämäärävalitsimen-kieli))
- Käännöksen tietoikkuna (sivupalkista **Peliasetukset > Käännöksen tiedot**), joka näyttää mm. onko käännös yhteensopiva nykyisen Foundry VTT version kanssa, onko TinyMCE käännös asennettu, sekä kääntäjien nimet.
- Näppäimistön näppäimien osittainen käännös.

Seuraavat ominaisuudet vaativat moduulin aktivoimisen maailmassa. ([Katso ohjeet alla.](#2-aktivoi-moduuli))
- [TinyMCE](https://www.tiny.cloud/ "TinyMCE verkkosivu.")-tekstinkäsittelyohjelman käyttöliittymän suomenkielinen käännös: kielen vaihtaminen vaatii erillisen käännöstiedoston lataamisen ([ohjeet alla](__REPO_TINYMCE_README_HEADER_FI__)).
  - Pelin TinyMCE:hen lisäämien valikkojen käännös.
  - TinyMCE-lisäosien valikkojen käännös. (Jotka *voi* kääntää, katso koodisäilön [ongelmanseurain](__REPO_ISSUES_URL__).)
  - Kun TinyMCE päivittyy, uudet kääntämättömät ominaisuudet näkyvät englanniksi.
- Karttamuistiinpanojen kuvakkeiden nimien käännös.
  - Kun kuvakkeet päivittyvät, uudet kääntämättömät nimet näkyvät englanniksi.
- Tutustumiskierrosten käännös.
- Kattavampi näppäimistön näppäimien käännös. Ominaisuuden saa pois päältä moduulin asetuksista, mikäli halutaan säilyttää näppäimien nimet osittain englanniksi.

## Nopea asennusohje
Mikäli sinulle kelpaa osittainen käyttöliittymän käännös, niin pelkkä moduulin asentaminen ja kielen vaihtaminen asetuksista riittää. Tarkemmat ohjeet tähän on alla.

### 1. Asenna moduuli
Helpoiten asennus onnistuu asentamalla moduuli `Translation: __MODULE_TITLE__` pelin liitännäismoduulien asentajasta (**Add-On Modules**&nbsp;-&NoBreak;välilehdeltä), jolloin saat uusimman version, joka on yhteensopiva sinun Foundry VTT versiosi kanssa. Mikäli haluat asentaa käännöksen tietylle vanhemmalle Foundry VTT:n versiolle käsin, löydät kaikki moduulin julkaistut versiot [Releases](__REPO_RELEASES_URL__)-sivulta.

Käännöksen uusimman version manifestin osoite on:
```
__REPO_MAIN_MANIFEST_URL__
```
Voit myös ladata uusimman ZIP-arkiston **[__MODULE_ID__.zip](__REPO_RELEASE_DOWNLOAD_URL__ "Moduulin ZIP-arkiston suora latauslinkki.")** ja purkaa sen sisällön hakemistoon: `<käyttäjäresurssien polku>/Data/modules/`

Käyttäjäresurssien polku löytyy ylläpitovalikon **Configuration** välilehdellä **User Data Path**&nbsp;-&NoBreak;kohdasta tai avaamalla Foundry VTT, napsauttamalla sovelluksen kuvaketta tehtäväpalkissa oikealla hiiren napilla ja valitsemalla **Browse User Data**.

### 2. Vaihda Foundry VTT:n kieli
Moduulin asentamisen jälkeen Foundry VTT:n *oletuskielen*, joka näkyy ylläpitovalikossa johon peli käynnistyy, voi vaihtaa suomeksi ylläpitovalikon **Configuration** välilehdeltä kohdasta **Default Language** valitsemalla `__LANG_NAME_FULL__ - __MODULE_TITLE__`. Tämän jälkeen sovellus pitää käynnistää uudelleen, jotta kieli vaihtuu.

Foundry VTT:n maailmoissa näkyvä *kieli* vaihdetaan avaamalla maailma ja valikosta **Game Settings > Configure Settings** valitse **Language Preference** kohdasta kieleksi `__LANG_NAME_FULL__`. Jos kielen vaihtaminen maailman asetuksissa ei muuta käyttöliittymän kieltä maailmassa, pyydä pelinjohtajaasi asentamaan ja/tai aktivoimaan tämä moduuli maailmassa.

## Laajennettu asennusohje
<img src="/dist/img/date_picker.png" alt="Käännetty päivämäärävalitsin." align="right">

Mikäli haluat kokonaisemman käännöksen, seuraa alla olevia ohjeita yllä olevan [nopean asennusohjeen](#nopea-asennusohje) *lisäksi*.

### 1. Vaihda päivämäärävalitsimen kieli
Päivämäärävalitsimen (joka löytyy esimerkiksi maailman muokkausikkunasta) kieli vaihtuu käyttöjärjestelmän kielen mukaan. Voit joko vaihtaa käyttöjärjestelmäsi kieleksi suomen tai muokata Foundry VTT:n pikakuvakkeen polkua.
1. Klikkaa sovelluksen kuvaketta oikealla hiiren napilla, valitse **Properties** (tai vastaava).
1. Etsi kuvakkeen polku (esim. **Target**-kenttä) ja lisää sen perään ` --lang=__LANG_TAG_SHORT_UPPER__`, esimerkiksi tähän tapaan:
```
"C:\Foundry VTT\FoundryVTT.exe" --lang=__LANG_TAG_SHORT_UPPER__
```

### 2. Aktivoi moduuli
Tämä moduuli "`__MODULE_TITLE__`" tulee aktivoida jokaisessa maailmassa, jossa haluat TinyMCE-tekstinkäsittelyohjelman ja karttamuistiinpanojen kuvakkeet suomeksi. Vain **pelinjohtajat** voivat aktivoida moduuleja. Aktivointi tapahtuu ruudun oikeanpuoleisen valikon kautta **Peliasetukset > Hallitse moduuleja** napilla. Valitse `__MODULE_TITLE__` ja paina **Tallenna muutokset**. Moduulin aktivoiminen myös mahdollistaa tilan, jossa päävalikko on yhdellä kielellä ja maailmassa käyttöliittymä on suomeksi.

<img src="/dist/img/tinymce.png" alt="Käännetty TinyMCE-tekstinkäsittelyohjelma." align="right">

### 3. Lataa TinyMCE käännös
Epäselvän lisensoinnin takia tämä moduuli ei voi sisällyttää TinyMCE-tekstinkäsittelyohjelman suomenkielistä kielipakettia, vaan se pitää ladata erikseen tästä suorasta latauslinkistä: [https://download.tiny.cloud/tinymce/community/languagepacks/6/__LANG_TAG_SHORT__.zip](https://download.tiny.cloud/tinymce/community/languagepacks/6/__LANG_TAG_SHORT__.zip "Suora latauslinkki viralliseen TinyMCE:n suomenkieliseen kielipakettiin.")

Pura `__LANG_TAG_SHORT__.js` tiedosto ladatusta arkistosta polkuun:
```
<käyttäjäresurssien polku>/Data/modules/__MODULE_ID__/lang/tinymce/__LANG_TAG_SHORT__.js
```
Päivitä peli (<kbd>F5</kbd>) ladataksesi TinyMCE käännöksen, jos teet tämän kun peli on päällä.

## Usein kysytyt kysymykset
Alla on vastauksia yleisiin ongelmiin, joita voi esiintyä moduulin asentamisen jälkeen.

- Miksi Foundry VTT:n ylläpitovalikko/päävalikko ei ole suomeksi?
- Miksi Foundry VTT ei ole suomeksi kun pelaan peliä maailmassa?
  - Et ole vaihtanut Foundry VTT:n *oletuskieltä* tai *kieltä* suomeksi. Katso ohjeet kohdasta [Vaihda Foundry VTT:n kieli](#2-vaihda-foundry-vttn-kieli).
- Miksi en näe **Käännöksen tiedot**&nbsp;-&NoBreak;nappia sivupalkissa?
- Miksi TinyMCE-tekstinkäsittelyohjelma ei ole suomeksi?
- Miksi karttamuistiinpanojen kuvakkeet eivät ole suomeksi?
  - Kaikki nämä ongelmat ratkeavat aktivoimalla moduuli. Katso ohjeet kohdasta [Aktivoi moduuli](#2-aktivoi-moduuli).

## Tunnetut ongelmat
- Kaikki teksti käyttöliittymässä ei ole käännetty: Foundry VTT ei vielä tue kaiken tekstin kääntämistä. Voimme vain odottaa sovelluksen päivittyvän tulevaisuudessa.
- Kaikki teksti TinyMCE tekstinkäsittelyohjelman käyttölittymässä ei ole käännetty: TinyMCE ja sen lisäosat eivät vielä tue kaiken tekstin kääntämistä. Voimme vain odottaa sovellusten päivittyvän tulevaisuudessa.
- Valitun tekstin raahaaminen käännöksen tietoikkunassa aiheuttaa virheitä selaimen konsolissa: tämä on pieni tekninen virhe, joka ei haittaa moduulin toiminnallisuutta.
- Työkaluvinkki ei näy käännöksen tietoikkunan ulkopuolella: tämä on pieni tekninen virhe, joka ei haittaa moduulin toiminnallisuutta.

## Yhteensopivuus
Tämän suomenkielisen käännöksen yhteensopivuus Foundry VTT:n versioiden kanssa näet [Releases](__REPO_RELEASES_URL__)-sivulta, sekä [CHANGELOG.md](/module/CHANGELOG.md)-tiedostosta. Tavoitteena on julkaista käännös jokaiselle Foundry VTT:n versiolle version 0.7.5 jälkeen.

## Osallistu kääntämiseen
Katso [CONTRIBUTING.md](/CONTRIBUTING.md).

## Kuvankaappauksia
![Käännetty hahmoluettelo.](/dist/img/actors_directory.png)
![Käännetty kohtauksen määritysikkuna.](/dist/img/scene_configuration.png)
![Käännetty karttamuistiinpanon määritysikkuna.](/dist/img/map_note_configuration.png)


# In English (englanniksi)
<img src="/dist/img/module_information_window.png" alt="Translation information window." align="right">

Please note that Foundry VTT *does not yet* support translating all text in the user interface. Untranslated text is in English. If the game has recently updated, please give the translators some time to update the translation. You can see the currently installed translation's latest supported Foundry VTT version by clicking the **Käännöksen tiedot** button in the sidebar's **Peliasetukset** tab if the module has been activated. This module **does not** contain translations for other packages: modules, game systems, or worlds.

For additional Finnish community translations for modules, game systems, and worlds see the [Foundry Hub Weblate](https://weblate.foundryvtt-hub.com/languages/__LANG_TAG_SHORT__/) or the **Foundry VTT Suomi** project on [GitLab](https://gitlab.com/foundry-vtt-suomi "Foundry VTT Suomi GitLab project page.") and [GitHub](https://github.com/foundry-vtt-suomi "Foundry VTT Suomi GitHub organization."). Contact the translators, give feedback, or report issues with this translation on the [__DISCORD_SERVER_NAME__ Discord server](__DISCORD_INVITE_LINK__ "__DISCORD_SERVER_NAME__ Discord server invite link."). You may also report issues with this translation in the repository's [issue tracker](__REPO_ISSUES_URL__).

Also, see the Finnish version of the [Foundry VTT Community Wiki](https://foundryvtt.wiki/__LANG_TAG_SHORT__/home "Finnish Foundry VTT Community Wiki").

## Features
- Translation of the user interface. (100% of the text that *can be* translated, see the repository's [issue tracker](__REPO_ISSUES_URL__))
  - Changing the date picker widget's language requires modifying the game shortcut. ([See instructions below.](#1-change-the-date-picker-widget-language))
- Translation information window (**Peliasetukset > Käännöksen tiedot** from the sidebar) that shows e.g., is the translation compatible with the current Foundry VTT version, is the TinyMCE translation installed, the names of the translators.
- Partial translation of keyboard keys.

The following features require activating the module in a world. ([See instructions below.](#1-change-the-date-picker-widget-language))
- [TinyMCE](https://www.tiny.cloud/ "TinyMCE website.") rich text editor Finnish translation: requires downloading a separate language pack ([instructions below](__REPO_TINYMCE_README_HEADER_EN__))
  - Translates all custom menus added to TinyMCE.
  - Translates all menus added by TinyMCE. (That *can be* translated, see the repository's [issue tracker](__REPO_ISSUES_URL__))
  - When TinyMCE updates, newly added untranslated options will be displayed in English.
- Translation of all map note icon names.
  - When map notes update, newly added untranslated icons will be displayed in English.
- Translation of tours.
- A more comprehensive translation of keyboard keys. The feature can be disabled in module settings, if you wish the keep the key names partially in English.

## Quick Installation Guide
If you're fine with a partial user interface translation, simply installing the module and changing the language from the settings is enough. More detailed instructions for this are provided below.

### 1. Install the Module
The easiest way to install the module is to install `Translation: __MODULE_TITLE__` from the in-game add-on installer (in the **Add-On Modules** tab). This way you will get the latest version of the module for your current Foundry VTT version. If you want to install the translation for a specific older version of Foundry VTT manually you will find all released versions of the module from the [Releases](__REPO_RELEASES_URL__) page.

The manifest URL for the newest module version is:
```
__REPO_MAIN_MANIFEST_URL__
```
You can also download the newest ZIP archive **[__MODULE_ID__.zip](__REPO_RELEASE_DOWNLOAD_URL__ "Direct download link for the module's ZIP archive.")** and extract its contents into the directory: `<user data path>/Data/modules/`

The user data path can be found in the setup menu's **Configuration** tab in the field **User Data Path** or by opening Foundry VTT, clicking the game's icon in the task bar with the right mouse button, and choosing **Browse User Data**.

### 2. Change Foundry VTT Language
After installing the module change Foundry VTT's *default language* (used in the setup menu) to Finnish in the setup menu's **Configuration** tab by choosing `__LANG_NAME_FULL__ - __MODULE_TITLE__` as the **Default Language**. After this the application has to be restarted for the language to change.

Change the *language* used in Foundry VTT worlds by launching a world and change the **Preferred Language** to `__LANG_NAME_FULL__` from **Game Settings > Configure Settings**. If changing the language in the world settings does not change the user interface language in the world, ask your Game Master to install or enable this module in the current world.

## Complete Installation Guide
<img src="/dist/img/date_picker.png" alt="Translated date picker widget." align="right">

If you want a more complete translation, follow the instructions below *in addition* to the [Quick Installation Guide](#quick-installation-guide) above.

### 1. Change the Date Picker Widget Language
The date picker widget (for example in the edit world window) language changes based on the operatin system language. You can either change your operating system language to Finnish or modify the path of the Foundry VTT shortcut.
1. Click the application shortcut with the right mouse button and choose **Properties** (or similar).
1. Find the shortcut's path (e.g., the **Target** field) and add ` --lang=__LANG_TAG_SHORT_UPPER__` to the end, gor example, like this:
```
"C:\Foundry VTT\FoundryVTT.exe" --lang=__LANG_TAG_SHORT_UPPER__
```

### 2. Activate the Module
**In addition to this** the module `__MODULE_TITLE__` must be activated in every world where you want the TinyMCE text editor and map note icon names to be in Finnish. Only **Game Masters** can activate modules. To activate modules go to **Game Settings > Manage Modules** in the right sidebar. Select `__MODULE_TITLE__` and press **Save Module Settings**.

If changing the language in world settings does not change the language of the user interface in that world, ask your Game Master to install this module and/or activate it in this world.

<img src="/dist/img/tinymce.png" alt="Translated TinyMCE text editor." align="right">

### 3. Download a TinyMCE Language Package
Due to unclear licensing this module cannot include the Finnish language package for the TinyMCE editor, and it has to be downloaded separately from this direct link: [https://download.tiny.cloud/tinymce/community/languagepacks/6/__LANG_TAG_SHORT__.zip](https://download.tiny.cloud/tinymce/community/languagepacks/6/__LANG_TAG_SHORT__.zip "Direct download link to the official TinyMCE Finnish language package.")

Extract the file `__LANG_TAG_SHORT__.js` from the downloaded archive to the path:
```
<user data path>/Data/modules/__MODULE_ID__/lang/tinymce/__LANG_TAG_SHORT__.js
```
Refresh the game (<kbd>F5</kbd>) to load the TinyMCE translation if you do this while the game is running.

## Frequently Asked Questions (FAQ)
- Why is Foundry VTT's setup menu/main menu not in Finnish?
- Why is Foundry VTT not in Finnish when playing a game in a world?
  - You have not changed Foundry VTT's *default language* or *language* to Finnish. See instructions in [Change Foundry VTT Language](#3-change-foundry-vtt-language).
- Why can I not see the **Käännöksen tiedot** button in the sidebar?
- Why is the TinyMCE rich text editor not in Finnish?
- Why are the map note icons not in Finnish?
  - Solve all of these issues by activating the module. See instructions in [Activate the Module](#2-activate-the-module).

## Known Issues
- Some text in the user interface is not translated: Foundry VTT does not support translating all text yet. We can only wait for the game to be updated.
- Some text in the TinyMCE interface is not translated: TinyMCE and its plugins do not support translating all text yet. We can only wait for the game to be updated.
- Dragging text in the module information window causes a console error: this is a minor technical issue that does not affect the functionality of the module.
- The tooltip does not show outside the module information window: this is a minor technical issue that does not affect the functionality of the module.

## Combatibility
The [Releases](__REPO_RELEASES_URL__) page and the [CHANGELOG.md](/module/CHANGELOG.md) file show the compatibility of this Finnish translation with the different Foundry VTT versions. The goal is to release a translation for each Foundry VTT version after 0.7.5.

## Contributing
See [CONTRIBUTING.md](/CONTRIBUTING.md).

## Screenshots
![Translated actors directory.](/dist/img/actors_directory.png)
![Translated scene configuration window.](/dist/img/scene_configuration.png)
![Translated map note configuration window.](/dist/img/map_note_configuration.png)
