# Muutokset

## 17.0.1

* Korjaus manifestiin, nykyinen käännös toimii lisäksi myös Foundry VTT versiolle 10.312.

## 17.0.0

* Tuki Foundry VTT versioille 10.291 ja 10.303.

## 16.0.0

* Tuki Foundry VTT versiolle 10.290.
 
## 15.0.0

* Tuki Foundry VTT versioille 10.287 ja 10.288.

## 14.0.0

* Tuki Foundry VTT versiolle 10.286.

## 13.0.0

* Tuki Foundry VTT versioille 10.284 ja 10.285.
* Korjattu soittolistan nimikkeet, jotka aiemmin rivittyivät kahdelle riville.
* Kaikki vanhat käyttämättömät käännökset poistettu: moduulin koko pieneni.
* Käännöksen avaimet järjestetty vastaamaan englanninkielistä lähdetiedostoa.

## 12.0.0

* Tuki Foundry VTT versioille 10.282 ja 10.283.
* Ehostuksia vanhoihin käännöksiin.
* Näppäimistön näppäimien käännös.
* Uusi moduulin asetus, jolla näppäimistön näppäinten käännöksen saa pois päältä.
* Pieniä teknisiä parannuksia.

## 11.0.0

* Tuki Foundry VTT versioille 9.268, 9.269 ja 9.280.

## 10.0.0

* Tuki Foundry VTT versiolle 9.266.

## 9.0.0

* Tuki Foundry VTT versioille 9.251, 9.254 ja 9.255.

## 8.0.0

* Tuki Foundry VTT versioille 9.245 ja 9.249.
* Kirjoitusvirhe korjattu.

## 7.0.0

* Tuki Foundry VTT versioille 9.240, 9.241 ja 9.242.

## 6.0.0

* Tuki Foundry VTT versiolle 9.238.
* Ehostuksia vanhoihin käännöksiin.

## 5.0.0

* Tuki Foundry VTT versiolle 0.8.9.
* Pieniä ehostuksia käännöksiin.
* Korjattu pieni kirjoitusvirhe edellisestä versiosta.

## 4.0.0

* Tuki Foundry VTT versioille 0.8.7 ja 0.8.8.
* Lisätty TinyMCE:n virallisesta käännöksestä puuttuva yksi käännös.
* Korjattu pieni kirjoitusvirhe edellisestä versiosta.
* Korjattu huono käännös ohjainten ohjeissa.

## 3.0.0

* Tuki Foundry VTT versioille 0.8.4, 0.8.5 ja 0.8.6.

## 2.0.0

* Tuki Foundry VTT versioille 0.7.9 ja 0.7.10.

## 1.0.0

* Tuki Foundry VTT versioille 0.7.5, 0.7.6, 0.7.7 ja 0.7.8.
* TinyMCE:n käännös ei toimi versiossa 0.7.5, mutta toimii versiosta 0.7.6 alkaen.

## 0.4.0

* README:tä ehostettu.
* CI korjattu.

## 0.3.0

* Muutosloki lisätty julkaisuihin.
* Versiointia muutettu.
* README lisäyksiä.
* `compatibleCoreVersion` arvo korjattu takaisin.
* CI korjauksia.

##
