# Foundry VTT Finnish Translation Module Development
[![MIT License](https://img.shields.io/badge/License-MIT-lightgray)](https://opensource.org/licenses/MIT "MIT License")

Tässä kansiossa olevaa kehityskoodia **ei voi** asentaa Foundry VTT:hen moduulina. Napsauta [**TÄSTÄ**](/dist "Foundry VTT Suomenkielinen käännös (Finnish Translation).") siirtyäksesi suomenkielisen käännösmoduulin kansioon.

The development code in this directory **cannot** be installed as a Foundry VTT module. Click [**HERE**](/dist "Foundry VTT Suomenkielinen käännös (Finnish Translation) module repository.") to move to the Finnish translation module directory.

---

This is a Node.js application that will merge and customize various `.JSON` files and build [a Foundry VTT module](/dist "Foundry VTT Suomenkielinen käännös (Finnish Translation) module repository.") that contains a translation for the core Foundry VTT software. In addition to the translation the module implements a popup dialog displaying the status of the translation for users. The build process is quite customizable and is configured using settings in [`module.config.json`](/module.config.json).

## Notable Features for Other Developers
- There is logic for **merging** multiple translation `.JSON` files ([`/module/lang`](/module/lang)) into a single `.JSON` file during the build process. The intention was to split the core Foundry VTT translation, and the module information window translation into separate files for easier maintenance. ([`rollup.config.js`](/rollup.config.js))
- The GUI code adds a button to the **Game Settings** tab to open a translation information window. (`addMenuButtons` in [`ui.js`](/module/modules/ui.js))
  - The window displays some basic information about the module version, authors, and potential incompatibility with the current Foundry VTT version. ([`ui.js`](/module/modules/ui.js), [`info.html`](/module/templates/info.html))
- There is JavaScript and CSS for a fake button that copies the text within the button (or custom text) to the clipboard. It has a fancy tooltip, and a simple fallback to select all text within the button if the clipboard is not accessible. The intention was to save the user from manually selecting the text and copying it. (`clickCopy` in [`handlebars_helpers.js`](/module/modules/handlebars_helpers.js), `addClickCopyListeners` in [`ui.js`](/module/modules/ui.js))
- Various convenient helper functions for Handlebars. ([`handlebars_helpers.js`](/module/modules/handlebars_helpers.js))

## Usage
- Install dependencies with `npm install`.
- Run `npm run dist` to build the final Foundry VTT module for a new release into the [`dist`](/dist) directory. You can also run `npm run dev` to build sourcemaps for easier debugging.
- Symlink to the `dist` directory with `mklink` on Windows:
```
mklink /D <user data path>/Data/modules/lang-fi-fi <full path>/fvtt-lang-fi-fi/dist/lang-fi-fi
```
- Activate the `Suomi [ydin] | Finnish [Core]` module in a world.

### Using for Other Translations
As per the repository license you are free to use the source code in this repository for your own projects. A link back to this repository is appreciated but not required. If you'd like to make a new Foundry VTT translation module based on this project, the following files will be of interest to you and will need to be modified.

#### Repository Files
- [`/module.config.json`](/module.config.json): This is the **primary configuration file** for the project. It is a `.JSON` file where the **key** can be used as a placeholder value in *most files* inside the `copy`, `lang`, `module`, or `templates` directories. The placeholder will be replaced with the corresponding **value** from the configuration file during build. See which files are passed through the `replaceStrings` function in [`rollup.config.js`](/rollup.config.js) for a complete list of supported files.
- [`/package.json`](/package.json): Metadata about the repository for npm.
- [`/module/copy/img`](/module/copy/img): The [module's README.md](/dist/README.md) images.

#### Foundry VTT Translation Files
- [`/module/lang/fvtt/icons.json`](/module/lang/fvtt/icons.json): Map note icon names.
- These files will be merged into a single translation file referenced in the `module.json`.
  - [`/module/lang/fvtt/translation.json`](/module/lang/fvtt/translation.json): Core Foundry VTT translation. The file that core translation modules usually only translate.
  - [`/module/lang/module/lang.json`](/module/lang/module/lang.json): Translation information window translation. The dialog added by this module is not required for using this module. It will fall back to English in case of a missing translation.
- [`/module/sass/translation.scss`](/module/sass/translation.scss): CSS styles required for the Foundry VTT & TinyMCE translation to display correctly. This should be modified as needed for each different language: sometimes a translated string is significantly longer than the original English one, so an element may need more space in the UI. This is the location to modify CSS styles to achieve this.

#### TinyMCE Translation Files
- [`/module/lang/tinymce/`](/module/lang/tinymce): Directory to place the TinyMCE `.js` language package during development. See the [module's README.md](/dist/README.md) to read about the licensing issue.
- These files will be merged into a single TinyMCE translation file which is loaded dynamically.
  - [`/module/lang/tinymce/fvtt.json`](/module/lang/tinymce/fvtt.json): Custom menus added by Foundry VTT.
  - [`/module/lang/tinymce/plugins.json`](/module/lang/tinymce/plugins.json): Custom menus added by TinyMCE plugins that are included in Foundry VTT.

#### Module Translation Files
- [`/module/lang/module-other/en.json`](/module/lang/module-other/en.json): Translation information window English backup strings.

## Background

For a simple Foundry VTT core translation module this project is rather overengineered. The reason for this is that the developer had decided on the following four principles to build this software on:
1. The desire was to practice writing ES6 modules, build scripts, as well as basic Foundry VTT API usage as this was the developer's first Foundry VTT module.
1. The intention was to make this repository reusable by non-developers for other translation modules by simply swapping out the translation files and editing some configuration files.
1. The ambition was to keep it simple and focused: no extra bells and whistles. It's a core translation, it does not need to stand out. What few features it has should be rock solid and blend well with the application itself.
1. The goal was for the project to be easier to maintain by avoiding a single massive JavaScript file and utilizing good coding conventions.

After implementing all desired features, completing development, and coming back to the code many, many months later they had realized the true complexity of the software after looking at it with fresh eyes and an empty mind.

The first and third principles were achieved as the developer learned a great deal about ES6 and Rollup build scripts. They also feel like they had managed to keep down feature bloat.

The second pricinple was arguably dead from the get go. Surely most people know how to install Node.js? Find specific `.json` files in a directory structure? They'll understand which lines to edit and remove? It shouldn't be too hard to a run a command in a command line interface? https://xkcd.com/2501/

The fourth principle is perhaps more a matter of taste.

This relies on heavy utilization of string replacements. Software developers should be able to reuse this repository for other translation modules without too much effort, but it certainly did not reach the initially desired end goal for reusability.

## Technologies & Libraries
Built on top of [typhonjs-fvtt/demo-rollup-module](https://github.com/typhonjs-fvtt/demo-rollup-module "demo-rollup-module GitHub repository.") v1.1.0.

- [ES6 modules](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules "Mozilla Developer Network article in JavaScript modules.") for neater JavaScript.
- [Node.js](https://nodejs.org/en/ "Node.js website.") for running the build scripts.
  - [fs-extra](https://github.com/jprichardson/node-fs-extra "Node.js fs-extra library GitHub repository.") for file management during the build process.
- [Rollup](https://www.rollupjs.org/guide/en/ "Rollup website.") module bundler for building JavaScript modules, the distributable module and repository file structure.
  - [rollup-plugin-copy](https://github.com/vladshcherbin/rollup-plugin-copy "Rollup copy plugin GitHub repository.") for file management during the build process.
  - [rollup-plugin-delete](https://github.com/vladshcherbin/rollup-plugin-delete "Rollup delete plugin GitHub repository.") for file management during the build process.
  - [rollup-plugin-json](https://github.com/rollup/plugins/tree/master/packages/json "Rollup json plugin GitHub repository.") for loading `.JSON` files as ES6 modules to reduce duplicate data.
  - [rollup-plugin-merge](https://github.com/zont/rollup-plugin-merge "Rollup merge plugin GitHub repository.") for merging translation `.JSON` files for improved version control and cleaner organization.
  - [rollup-plugin-replace](https://github.com/rollup/plugins/tree/master/packages/replace "Rollup replace plugin GitHub repository.") utilized heavily for string replacements to massively reduce duplicated text and potential copy-paste errors.
  - [rollup-plugin-postcss](https://github.com/egoist/rollup-plugin-postcss "Rollup postcss plugin GitHub repository.") to process Sass.
    - [autoprefixer](https://github.com/postcss/autoprefixer "Autoprefixer GitHub repository.") to reduce duplicate CSS.
- [Sass](https://sass-lang.com/ "Sass website.") CSS preprocessor for easier to maintain CSS.
