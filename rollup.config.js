import path from "path";
import dotenv from "dotenv-safe"; // Provides flexible definition of environment variables.
import autoprefixer from "autoprefixer"; // PostCSS
import postcssPresetEnv from "postcss-preset-env"; // PostCSS
import postcssFailOnWarn from "postcss-fail-on-warn"; // PostCSS
import copy from "rollup-plugin-copy"; // Copies files
import postcss from "rollup-plugin-postcss"; // Process Sass / CSS w/ PostCSS
import replace from "@rollup/plugin-replace"; // Replaces text in processed source files.
import merge from "rollup-plugin-merge"; // Merge JSON files.
import module_conf from "./module.config.json"; // FVTT module configuration/string replacements.
import del from "rollup-plugin-delete"; // Delete temp files.
import fse from "fs-extra";

export default () => {
    "use strict";
    // Load the .env file specified in the command line target into process.env using `dotenv-safe`
    // This is a very convenient and cross platform way to handle environment variables. Please note that the .env
    // files are committed to this repo, but in your module you should uncomment the `**.env` directive in `.gitignore`
    // so that .env files are not committed into your repo. The difference between `dotenv-safe` and `dotenv` is that
    // the former provides a template providing a sanity check for imported .env files to make sure all required
    // parameters are present. This template is located in `./env/.env.example` and can be checked into your repo.

    // There are two environment variables loaded from .env files.
    // process.env.FVTTDEV_DEPLOY_PATH is the full path to the destination of your module / bundled code.
    // process.env.FVTTDEV_COMPRESS specifies if the bundled code should be minified.
    // process.env.FVTTDEV_SOURCEMAPS specifies if the source maps should be generated.

    // process.env.TARGET is defined in package.json NPM scripts using the `cross-env` NPM module passing it into
    // running this script. It defines which .env file to use below.
    dotenv.config({
        example: `${__dirname}${path.sep}env${path.sep}.env.example`,
        path: `${__dirname}${path.sep}env${path.sep}${process.env.TARGET}.env`
    });

    // Sanity check to make sure parent directory of FVTTDEV_DEPLOY_PATH exists.
    if (!fse.existsSync(path.dirname(process.env.FVTTDEV_DEPLOY_PATH)))
        throw Error(`FVTTDEV_DEPLOY_PATH does not exist: ${process.env.FVTTDEV_DEPLOY_PATH}`);

    const rtrim = (str, trim) => str.replace(new RegExp(`${trim}*$`), "");

    const TEMP_ROOT = rtrim(process.env.FVTTDEV_TEMP_PATH, "/");
    const DIST_ROOT = rtrim(process.env.FVTTDEV_DEPLOY_PATH, "/");
    const DIST_DIR_NAME = DIST_ROOT.slice(2);
    const DIST_MODULE_ROOT = `${DIST_ROOT}/${module_conf.__MODULE_ID__}`;
    const DIST_MODULE_LANG = `${DIST_MODULE_ROOT}/lang`;
    const DEV_ROOT = rtrim(process.env.FVTTDEV_DEVELOPMENT_ROOT, "/");
    const DEV_COPY = `${DEV_ROOT}/copy`;
    const DEV_LANG = `${DEV_ROOT}/lang`;

    const parseRepoHost = (repo_url) => {
        const low_url = repo_url.toLowerCase();

        if (low_url.includes("gitlab.com/"))
            return "GitLab";
        else if (low_url.includes("github.com/"))
            return "GitHub";
        else {
            console.warn(`Unknown repository host for URL: ${repo_url}`);
            return "Unknown";
        }
    };

    const getRepoURLs = (repo_host, repo_url, main_branch, release_branch, module_id, module_version) => {
        if ("GitLab" === repo_host) {
            const repo_release_url = `${repo_url}/-/releases`;
            const repo_raw_url = `${repo_url}/-/raw`;
            const repo_blob_url = `${repo_url}/-/blob`;

            return {
                main_manifest: `${repo_release_url}/permalink/latest/downloads/module.json`,
                release_manifest: `${repo_raw_url}/${module_version}/${DIST_DIR_NAME}/${module_id}/module.json`,
                release_download: `${repo_release_url}/${module_version}/downloads/${module_id}.zip`,
                release_license: `${repo_blob_url}/${module_version}/${DIST_DIR_NAME}/LICENSE`,
                release_readme: `${repo_blob_url}/${module_version}/${DIST_DIR_NAME}/README.md`,
                issues: `${repo_url}/-/issues`,
                releases: `${repo_release_url}`,
                branches: `${repo_url}/-/branches`,
                main_dist_dir: `${repo_url}/-/tree/${main_branch}/${DIST_DIR_NAME}`,
                release_raw_img_dir: `${repo_raw_url}/${module_version}/${DIST_DIR_NAME}/img`
            };
        } else if ("GitHub" === repo_host) {
            const github_url = "github.com";
            const github_repo_raw_base = `https://raw.githubusercontent.com${repo_url.substring(repo_url.indexOf(github_url) + github_url.length)}`;

            return {
                main_manifest: `${github_repo_raw_base}/${main_branch}/${DIST_DIR_NAME}/${module_id}/module.json`,
                release_manifest: `${github_repo_raw_base}${release_branch}/${DIST_DIR_NAME}/${module_id}/module.json`,
                release_download: `${repo_url}/releases/download/v${module_version}/module.zip`,
                release_license: `${github_repo_raw_base}/${module_version}/LICENSE`,
                release_readme: `${repo_url}/blob/${module_version}/${DIST_DIR_NAME}/README.md`,
                issues: `${repo_url}/issues`,
                releases: `${repo_url}/releases`,
                branches: `${repo_url}/branches`,
                main_dist_dir: `${repo_url}/tree/${main_branch}/${DIST_DIR_NAME}`,
                release_raw_img_dir: `${github_repo_raw_base}/${release_branch}/img`
            };
        } else {
            console.warn(`Unknown repository host '${repo_host}'. Cannot build repository URLs.`);
            return {};
        }
    };

    // Reverse relative path from the deploy path to local directory; used to replace source maps path.
    const relativePath = path.relative(process.env.FVTTDEV_DEPLOY_PATH, ".");

    // Defines whether source maps are generated / loaded from the .env file.
    const sourcemap = "true" === process.env.FVTTDEV_SOURCEMAPS;

    // Strip / from the end of paths if present.
    module_conf.__REPO_BASE_URL__ = rtrim(module_conf.__REPO_BASE_URL__, "/");
    module_conf.__REPO_HOST__ = parseRepoHost(module_conf.__REPO_BASE_URL__);
    module_conf.__BUILD_TYPE__ = process.env.TARGET.split("-")[1];
    module_conf.__MODULE_PATH__ = `modules/${module_conf.__MODULE_ID__}`;
    module_conf.__MODULE_COLOR__ = module_conf.__MODULE_COLOR__ || "#000000";
    module_conf.__TINYMCE_LANG_ROOT__ = `${module_conf.__MODULE_PATH__}/lang/tinymce`;
    module_conf.__LANG_NAME_FULL__ = `${module_conf.__LANG_NAME_LOCAL__} (${module_conf.__LANG_NAME_EN__})`;
    module_conf.__LANG_TAG_SHORT_UPPER__ = module_conf.__LANG_TAG_SHORT__.toUpperCase();

    const repo_urls = getRepoURLs(module_conf.__REPO_HOST__,
        module_conf.__REPO_BASE_URL__,
        module_conf.__REPO_MAIN_BRANCH_NAME__,
        `${module_conf.__GITHUB_REPO_VERSION_BRANCH_PREFIX__}${module_conf.__MODULE_VERSION__}`,
        module_conf.__MODULE_ID__,
        module_conf.__MODULE_VERSION__);

    module_conf.__REPO_MAIN_MANIFEST_URL__ = repo_urls.main_manifest;
    module_conf.__REPO_RELEASE_DOWNLOAD_URL__ = repo_urls.release_download;
    module_conf.__REPO_RELEASE_LICENSE_URL__ = repo_urls.release_license;
    module_conf.__REPO_RELEASE_README_URL__ = repo_urls.release_readme;
    module_conf.__REPO_ISSUES_URL__ = repo_urls.issues;
    module_conf.__REPO_RELEASES_URL__ = repo_urls.releases;
    module_conf.__REPO_BRANCHES_URL__ = repo_urls.branches;
    module_conf.__REPO_DIST_URL__ = repo_urls.main_dist_dir;
    module_conf.__REPO_RELEASE_MANIFEST_URL_URLENCODED__ = encodeURIComponent(repo_urls.release_manifest);
    module_conf.__REPO_TINYMCE_README_URL_FI__ = `${repo_urls.release_readme}${module_conf.__REPO_TINYMCE_README_HEADER_FI__}`;
    module_conf.__REPO_TINYMCE_README_URL_EN__ = `${repo_urls.release_readme}${module_conf.__REPO_TINYMCE_README_HEADER_EN__}`;
    module_conf.__REPO_INSTALLATION_HEADER_FI__ = `${repo_urls.release_readme}${module_conf.__REPO_INSTALLATION_HEADER_FI__}`;
    module_conf.__REPO_INSTALLATION_HEADER_EN__ = `${repo_urls.release_readme}${module_conf.__REPO_INSTALLATION_HEADER_EN__}`;
    module_conf.__REPO_RELEASE_IMG_DIR__ = repo_urls.release_raw_img_dir;

    const replaceStrings = (contents) => {
        let result = contents.toString();

        for (const placeholder in module_conf) {
            if (Object.prototype.hasOwnProperty.call(module_conf, placeholder) && placeholder.startsWith("__"))
                result = result.replace(new RegExp(placeholder, "g"), module_conf[placeholder]);
        }

        return result;
    };

    const licenseReplace = (contents) => {
        let result = contents.toString();

        for (const placeholder in module_conf) {
            if (Object.prototype.hasOwnProperty.call(module_conf, placeholder) && placeholder.startsWith("__")) {
                if ("__MODULE_AUTHORS__" === placeholder)
                    result = result.replace(new RegExp(placeholder, "g"), JSON.parse(module_conf[placeholder]).map((author) => Object.prototype.hasOwnProperty.call(author, "discord") ? `${author.name} (Discord: ${author.discord})` : author.name).join(", "));
                else
                    result = result.replace(new RegExp(placeholder, "g"), module_conf[placeholder]);
            }
        }

        return result;
    };

    console.log(`Bundling target: ${process.env.TARGET}`);

    console.log("Modifying module.json...");

    const module_json = JSON.parse(fse.readFileSync(`${DEV_ROOT}/module.json`, "utf8"));

    if (Object.prototype.hasOwnProperty.call(module_conf, "__MODULE_AUTHORS__") && module_conf.__MODULE_AUTHORS__) {
        console.log("\tSet authors.");
        module_json.authors = module_conf.__MODULE_AUTHORS__;
        module_conf.__MODULE_AUTHORS__ = JSON.stringify(module_conf.__MODULE_AUTHORS__);
    }

    if (Object.prototype.hasOwnProperty.call(module_conf, "__MODULE_MEDIA__") && module_conf.__MODULE_MEDIA__) {
        console.log("\tSet media.");
        module_json.media = module_conf.__MODULE_MEDIA__;
        module_conf.__MODULE_MEDIA__ = JSON.stringify(module_conf.__MODULE_MEDIA__);
    }

    if (fse.existsSync(`${DEV_LANG}/module/en.json`)) {
        console.log("\tAdding English module translation.");

        module_json.languages.push({
            lang: "en",
            name: "English",
            path: "lang/en.json"
        });
    }

    console.log("\tClear temp directory.");
    fse.removeSync(TEMP_ROOT);

    console.log("\tWrite to module.json temp directory.");
    fse.outputFileSync(`${TEMP_ROOT}/module.json`, JSON.stringify(module_json, null, 2), { encoding: "utf8" });

    return [{
        input: `${DEV_ROOT}/init.js`,
        output: {
            file: `${DIST_MODULE_ROOT}/module.js`,
            format: "es",
            sourcemap,
            sourcemapPathTransform: (sourcePath) => sourcePath.replace(relativePath, `.`)
        },
        plugins: [
            del({
                verbose: false,
                hook: "buildStart",
                targets: [
                    `${DIST_ROOT}/**`,
                    `!${DIST_ROOT}`,
                    `!${DIST_ROOT}/.git`
                ]
            }),
            replace({
                exclude: "node_modules/**",
                delimiters: ["", ""],
                preventAssignment: false,
                values: module_conf
            }),
            postcss({
                inject: false,                  // Don't inject CSS into <HEAD>
                extract: "module.css",          // Output to `module.css` in directory of the bundle
                extensions: [".scss", ".sass"], // File extensions
                plugins: [autoprefixer, postcssPresetEnv, postcssFailOnWarn],
                use: ["sass"],                  // Use sass / dart-sass
                sourcemap
            }),
            merge({
                input: [
                    `${DEV_LANG}/fvtt/translation.json`,
                    `${DEV_LANG}/module/lang.json`
                ],
                output: `${TEMP_ROOT}/lang/${module_conf.__LANG_TAG_CANONICAL__}.json`,
                verbose: true,
                watch: false,
                recursive: false,
                prettify: true
            }),
            merge({
                input: [
                    `${DEV_LANG}/tinymce/fvtt.json`,
                    `${DEV_LANG}/tinymce/plugins.json`
                ],
                output: `${TEMP_ROOT}/lang-tinymce/${module_conf.__LANG_TAG_CANONICAL__}_custom.json`,
                verbose: true,
                watch: false,
                recursive: false,
                prettify: true
            }),
            copy({
                verbose: true,
                hook: "buildEnd",
                targets: [
                    {
                        src: `${DEV_COPY}/img`,
                        dest: `${DIST_ROOT}`
                    },
                    {
                        src: `${DEV_ROOT}/README.md`,
                        dest: DIST_ROOT,
                        // eslint-disable-next-line no-unused-vars
                        transform: (contents, filename) => replaceStrings(contents) // jshint ignore:line
                    },
                    {
                        src: `${DEV_ROOT}/CONTRIBUTING.md`,
                        dest: DIST_ROOT,
                        // eslint-disable-next-line no-unused-vars
                        transform: (contents, filename) => replaceStrings(contents) // jshint ignore:line
                    },
                    {
                        src: `${DEV_ROOT}/LICENSE`,
                        dest: DIST_ROOT,
                        // eslint-disable-next-line no-unused-vars
                        transform: (contents, filename) => licenseReplace(contents) // jshint ignore:line
                    },
                    {
                        src: `${DEV_ROOT}/.gitlab-ci.yml`,
                        dest: DIST_ROOT,
                        // eslint-disable-next-line no-unused-vars
                        transform: (contents, filename) => licenseReplace(contents) // jshint ignore:line
                    },
                    {
                        src: `${DEV_ROOT}/templates/*`,
                        dest: `${DIST_MODULE_ROOT}/templates/`,
                        // eslint-disable-next-line no-unused-vars
                        transform: (contents, filename) => replaceStrings(contents) // jshint ignore:line
                    },
                    {
                        src: `${DEV_LANG}/fvtt/icons.json`,
                        dest: `${DIST_MODULE_LANG}/fvtt`,
                        // eslint-disable-next-line no-shadow,no-unused-vars
                        rename: (name, extension, fullPath) => `${module_conf.__LANG_TAG_CANONICAL__}_${name}.${extension}` // jshint ignore:line
                    },
                    {
                        src: `${DEV_LANG}/fvtt/tours.json`,
                        dest: `${DIST_MODULE_LANG}/fvtt`,
                        // eslint-disable-next-line no-shadow,no-unused-vars
                        rename: (name, extension, fullPath) => `${module_conf.__LANG_TAG_CANONICAL__}_${name}.${extension}` // jshint ignore:line
                    },
                    {
                        src: `${DEV_LANG}/fvtt/keyboard.json`,
                        dest: `${DIST_MODULE_LANG}/fvtt`,
                        // eslint-disable-next-line no-shadow,no-unused-vars
                        rename: (name, extension, fullPath) => `${module_conf.__LANG_TAG_CANONICAL__}_${name}.${extension}` // jshint ignore:line
                    },
                    {
                        src: `${DEV_LANG}/module-other/*`,
                        dest: DIST_MODULE_LANG,
                        // eslint-disable-next-line no-unused-vars
                        transform: (contents, filename) => replaceStrings(contents) // jshint ignore:line
                    },
                    {
                        src: `${TEMP_ROOT}/lang-tinymce/*`,
                        dest: `${DIST_MODULE_LANG}/tinymce/`
                    },
                    {
                        src: `${TEMP_ROOT}/lang/*`,
                        dest: DIST_MODULE_LANG,
                        // eslint-disable-next-line no-unused-vars
                        transform: (contents, filename) => replaceStrings(contents) // jshint ignore:line
                    },
                    {
                        src: `${TEMP_ROOT}/module.json`,
                        dest: DIST_MODULE_ROOT,
                        // eslint-disable-next-line no-unused-vars
                        transform: (contents, filename) => replaceStrings(contents) // jshint ignore:line
                    }
                ]
            }),
            del({
                verbose: true,
                hook: "writeBundle",
                targets: TEMP_ROOT
            })
        ]
    }];
};
