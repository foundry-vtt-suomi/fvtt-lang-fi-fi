# Kääntäminen
Ota yhteyttä kääntäjiin [Foundry VTT Suomi](https://discord.gg/U4y3cNebbg "Foundry VTT Suomi -Discord-palvelimen kutsulinkki.") -Discord-palvelimella.

# Versiopäivitys
1. Päivitä versionumero `__MODULE_VERSION__`-kohtaan [module.config.json](/module.config.json)-tiedostossa. Seuraa [semanttisen versionnin](https://semver.org/ "Semantic Versioning -verkkosivu.") ohjeita.
1. Tee muutokset koodiin ja/tai [kielitiedostoihin](/module/lang "Linkki moduulin käännöstiedostoihin.").
1. Tee uusi otsikko versionumerosta [CHANGELOG.md](/module/CHANGELOG.md "Linkki CHANGELOG.md tiedostoon.")-tiedostoon ja kirjoita sen alle lista muutoksista suomeksi. Teknisesti tätä ei ole pakko tehdä, mutta se on erittäin suositeltavaa.
1. Kokoa uusi moduuli `npm run dist`-komennolla.
1. Sitouta muutokset ja kirjoita englanninkielinen kuvaus muutoksista.
1. Lisää sitoutukseesi uusi lippu, joka vastaa uutta versionumeroa.
1. Työnnä muutokset palvelimelle: GitLabin jatkuva integraatio tekee muutoksistasi uuden julkaisun [Releases](https://gitlab.com/foundry-vtt-suomi/fvtt-lang-fi-fi/-/releases "Linkki moduulin julkaisuihin.")-sivulle automaattisesti.

# Contributing
Get in touch with the translators on the [Foundry VTT Suomi](https://discord.gg/U4y3cNebbg "Foundry VTT Suomi Discord server invite link.")  Discord server.
